package ex8;
import java.util.Scanner;
public class GreatestAndLeastDriver {

	public static void main(String[] args) 
	{
		Scanner keyboard = new Scanner(System.in);
		
		int number = 0;
		int highestNum = -999999999;
		int lowestNum = 999999999;
		
		while(number != -99)
		{
			System.out.println("Enter a series of integers, enter '-99' when done.");
			number = keyboard.nextInt();
		
			if(number != -99)
			{
				if(number > highestNum) 
				{
					highestNum = number;
				}
				
				if(number < lowestNum)
				{
					lowestNum = number;
				}
			}
		}
		
		System.out.println("Highest Number: " + highestNum);
		System.out.println("Lowest Number: " + lowestNum);
		
		keyboard.close();
	}

}
