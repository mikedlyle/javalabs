package ex1;
import java.util.Scanner;

public class SumOfNumbersDriver {

	public static void main(String[] args) {
		Scanner keyboard = new Scanner(System.in);
		
		int input;
		System.out.println("Enter a positive integer: ");
		input = keyboard.nextInt();

		int sum = 0;
		for(int i = 1; i <= input; i++)
		{
			System.out.println(i);
			sum += i;
		}
		System.out.println("\nSum: " + sum);
		
		keyboard.close();
	}

}
