package ex6;

public class Population 
{
	private int numOrganisms;
	private double popIncrease;
	private double numDays;
	
	public Population() 
	{
		
	}

	public int getNumOrganisms() {
		return numOrganisms;
	}

	public void setNumOrganisms(int numOrganisms) {
		this.numOrganisms = numOrganisms;
	}

	public double getPopIncrease() {
		return popIncrease;
	}

	public void setPopIncrease(double popIncrease) {
		this.popIncrease = popIncrease;
	}

	public double getNumDays() {
		return numDays;
	}

	public void setNumDays(double numDays) {
		this.numDays = numDays;
	}
	
	public double sizePopulation()
	{
		
		for(int i = 1; i <= numDays; i++)
		{
			System.out.println("The size of the population on day " + i + " is " + numOrganisms);
			numOrganisms += numOrganisms * popIncrease;
		}
		
		return numOrganisms;
	}
}
