package ex6;
import java.util.Scanner;

public class PopulationDriver {

	public static void main(String[] args) 
	{
		Scanner keyboard = new Scanner(System.in);

		int numOrganisms = 0;
		double popIncrease = -1;
		double numDays = 0;
		
		while(numOrganisms < 2)
		{
			System.out.println("Enter the number of organisms: ");
			numOrganisms = keyboard.nextInt();
		}
		
		while(popIncrease < 0)
		{
		System.out.println("Enter the population increase as a percentage (ex. 0.5 would be 50 percent): ");
		popIncrease = keyboard.nextDouble();
		}
		
		while(numDays < 1)
		{
		System.out.println("Enter the number of days the organisms are multiplying: ");
		numDays = keyboard.nextDouble();
		}
		
		Population pop1 = new Population();
		
		pop1.setNumOrganisms(numOrganisms);
		pop1.setPopIncrease(popIncrease);
		pop1.setNumDays(numDays);
		
		System.out.println("------------\nThe total population: " + pop1.sizePopulation());
		keyboard.close();
	}

}
