package ex5;
import java.util.Scanner;

public class HotelOccupancyDriver {
	public static void main(String[] args) 
	{
		Scanner keyboard = new Scanner(System.in);
		
		int numFloors = 0;
		int numRooms = 0;
		int roomsOccupied = 0;
		int totalRooms = 0;
		int totalOccupied = 0;
		
		while(numFloors < 1)
		{
		System.out.println("Enter the number of floors that the hotel has.");
		numFloors = keyboard.nextInt();
		}
		
		for(int i = 1; i <= numFloors; i++)
		{
			
			System.out.println("Enter the number of rooms on floor " + i);
			numRooms = keyboard.nextInt();
			
			System.out.println("How many rooms occupied?");
			roomsOccupied = keyboard.nextInt();
			
			while(numRooms < 10)
			{
				System.out.println("Error: Enter ten or more for rooms.");
				System.out.println("Enter the number of rooms on that floor");
				numRooms = keyboard.nextInt();
			}
			
			totalRooms += numRooms;
			totalOccupied += roomsOccupied;
		}
		
		double occupancyRate = (double) roomsOccupied / numRooms;
		
		System.out.println("Total Rooms: " + totalRooms);
		System.out.println("Number of Rooms Occupied: " + (totalOccupied));
		System.out.println("Number of Rooms Vacant: " + (totalRooms - totalOccupied));
		System.out.printf("\nOccupancy Rate: %.2f", occupancyRate);
		
		keyboard.close();
	}
}
