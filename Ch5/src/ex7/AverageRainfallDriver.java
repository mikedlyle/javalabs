package ex7;
import java.util.Scanner;

public class AverageRainfallDriver {

	public static void main(String[] args) {
		Scanner keyboard = new Scanner(System.in);
		
		int numYears = 0;
		int months = 0;
		double inchesRainfall = -1;
		double totalRainfall = 0;
		double averageRainfall;
		
		while(numYears < 1)
		{
		System.out.println("Enter the number of years: ");
		numYears = keyboard.nextInt();
		}
		
		for(int i = 1; i <= numYears; i++)
		{
			for(int y = 1; y <= 12; y++)
			{
				while(inchesRainfall < 0)
				{
				System.out.println("Enter inches of rainfall for month " + y);
				inchesRainfall = keyboard.nextDouble();
				totalRainfall += inchesRainfall;
				}
				inchesRainfall = -1;

			}
			months = numYears * 12;
		}

		
		averageRainfall = totalRainfall / months;
		
		System.out.println("Number of months: " + months);
		System.out.println("Total inches of rain: " + totalRainfall);
		System.out.println("Average rainfall: " + averageRainfall);
		
		keyboard.close();
	}

}
