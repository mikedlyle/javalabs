package ex9;

public class PayrollReport 
{
	private int idNum;
	private double grossPay;
	private double stateTax;
	private double federalTax;
	private double ficaWithholdings;
	
	
	public int getIdNum() {
		return idNum;
	}
	public void setIdNum(int idNum) {
		this.idNum = idNum;
	}
	public double getGrossPay() {
		return grossPay;
	}
	public void setGrossPay(double grossPay) {
		this.grossPay = grossPay;
	}
	public double getStateTax() {
		return stateTax;
	}
	public void setStateTax(double stateTax) {
		this.stateTax = stateTax;
	}
	public double getFederalTax() {
		return federalTax;
	}
	public void setFederalTax(double federalTax) {
		this.federalTax = federalTax;
	}
	public double getFicaWithholdings() {
		return ficaWithholdings;
	}
	public void setFicaWithholdings(double ficaWithholdings) {
		this.ficaWithholdings = ficaWithholdings;
	}
	
	public double calcNetPay()
	{
		double netPay = grossPay - stateTax - federalTax - ficaWithholdings;
		
		return netPay;
	}
}
