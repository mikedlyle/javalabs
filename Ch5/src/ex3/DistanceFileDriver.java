package ex3;
import java.io.*;
import java.util.Scanner;

import ex2.DistanceTraveled;

public class DistanceFileDriver {

	public static void main(String[] args) throws IOException {
		PrintWriter distanceFile = new PrintWriter("distanceFile.txt");
		Scanner keyboard = new Scanner(System.in);
		
		double speedInput;
		double hoursInput;
		System.out.println("Enter speed of vehicle: ");
		speedInput = keyboard.nextDouble();
		
		System.out.println("Enter number of hours traveled: ");
		hoursInput = keyboard.nextDouble();
		
		DistanceTraveled dist1 = new DistanceTraveled(speedInput, hoursInput);
		
		if(speedInput <= 0 || hoursInput < 0)
		{
			System.out.println("Error: Enter a positive number.");
		}else
		{
			for(int hours = 1; hours <= hoursInput; hours++)
			{
				System.out.println("Hours: " + hours + " --- " + "Distance Traveled: " + (dist1.getDistance(dist1.getSpeed(), hours)));
				distanceFile.println("Hours: " + hours + " --- " + "Distance Traveled: " + (dist1.getDistance(dist1.getSpeed(), hours)));
			}
			System.out.println("Your output is printed in 'distanceFile.txt'");
			distanceFile.close();
		}
		
		keyboard.close();
	}

}
