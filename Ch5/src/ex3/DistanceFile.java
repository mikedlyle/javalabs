package ex3;

public class DistanceFile {
	private double speed;
	private double hours;
	
	public DistanceFile() 
	{
		
	}

	public DistanceFile(double speed, double hours)
	{
		this.speed = speed;
		this.hours = hours;
	}
	
	public double getSpeed() {
		return speed;
	}

	public void setSpeed(double speed) {
		this.speed = speed;
	}

	public double getHours() {
		return hours;
	}

	public void setHours(double hours) {
		this.hours = hours;
	}

	public double getDistance(double speed, double hours)
	{
		this.speed = speed;
		this.hours = hours;
		double distance = speed * hours;
		
		return distance;
	}
}
