package ex2;
import java.util.Scanner;

public class DistanceTraveledDriver {

	public static void main(String[] args) 
	{
		Scanner keyboard = new Scanner(System.in);
		
		double speedInput;
		double hoursInput;
		System.out.println("Enter speed of vehicle: ");
		speedInput = keyboard.nextDouble();
		
		System.out.println("Enter number of hours traveled: ");
		hoursInput = keyboard.nextDouble();
		
		DistanceTraveled dist1 = new DistanceTraveled(speedInput, hoursInput);
		
		if(speedInput <= 0 || hoursInput < 0)
		{
			System.out.println("Error: Enter a positive number.");
		}else
		{
			for(int hours = 1; hours <= hoursInput; hours++)
			{
				System.out.println("Hours: " + hours + " --- " + "Distance Traveled: " + (dist1.getDistance(dist1.getSpeed(), hours)));
			}
		}
		
		keyboard.close();
	}

}
