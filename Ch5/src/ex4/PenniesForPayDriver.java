package ex4;
import java.util.Scanner;

public class PenniesForPayDriver {

	public static void main(String[] args) {
		Scanner keyboard = new Scanner(System.in);
		
		double inputDays;
		
		System.out.println("Enter the number of days worked: ");
		inputDays = keyboard.nextInt();
		
		while(inputDays < 1)
		{
			System.out.println("Enter a positive number for days.");
		}
		
		double salary = 0.01;
		double total = 1;
		for(double i = 1; i <= inputDays; i++)
		{
			salary = salary * 2;
			System.out.printf("\nDay %.0f : " + (salary / 2), i);
			total += salary;
		}
		System.out.println("\n---------------\nTotal pay is: " + total);
		keyboard.close();
	}

}
