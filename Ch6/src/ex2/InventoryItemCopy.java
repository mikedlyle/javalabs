package ex2;

public class InventoryItemCopy {

	   private String description;
	   private int units;

	   public InventoryItemCopy()
	   {
	      description = "";
	      units = 0;
	   }

	   public InventoryItemCopy(String d)
	   {
	      description = d;
	      units = 0;
	   }
	   
	   public InventoryItemCopy(InventoryItemCopy item2)
	   {
		   description = item2.description;
		   units = item2.units;
	   }

	   public InventoryItemCopy(String d, int u)
	   {
	      description = d;
	      units = u;
	   }
	   
	   public void setDescription(String d)
	   {
	      description = d;
	   }

	   public void setUnits(int u)
	   {
	      units = u;
	   }

	   public String getDescription()
	   {
	      return description;
	   }

	   public int getUnits()
	   {
	      return units;
	   }
}

