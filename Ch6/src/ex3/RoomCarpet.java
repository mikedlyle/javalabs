package ex3;

public class RoomCarpet {
	private RoomDimension size;
	private double carpetCost;
	
	public RoomCarpet(RoomDimension size, double carpetCost)
	{
		this.size = size;
		this.carpetCost = carpetCost;
	}
	
	public RoomDimension getSize() {
		return size;
	}

	public void setSize(RoomDimension size) {
		this.size = size;
	}

	public double getCarpetCost() {
		return carpetCost;
	}

	public void setCarpetCost(double carpetCost) {
		this.carpetCost = carpetCost;
	}

	public double totalCost()
	{
		return carpetCost * size.getArea();
	}
	
	public String toString()
	{
		return size.toString() + "\nThe cost per square foot is " + carpetCost + "\nThe total cost for carpet in this room is $" + totalCost();
	}
}