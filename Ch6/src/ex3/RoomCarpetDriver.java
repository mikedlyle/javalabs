package ex3;
import java.util.Scanner;

public class RoomCarpetDriver {

	public static void main(String[] args) {
		Scanner keyboard = new Scanner(System.in);
		
		System.out.println("What is the width of the room?");
		double w = keyboard.nextDouble();
		
		System.out.println("What is the length of the room?");
		double l = keyboard.nextDouble();
		
		RoomDimension room1 = new RoomDimension(l, w);
		
		System.out.println("What is the cost per square foot?");
		double c = keyboard.nextDouble();
		
		RoomCarpet rc = new RoomCarpet(room1, c);
		
		System.out.println(rc.toString());
		
		keyboard.close();
	}

}
