package ex3;

public class RoomDimension {
	private double length;
	private double width;
	
	public RoomDimension(double l, double w)
	{
		this.length = l;
		this.width = w;
	}
	
	public double getLength() {
		return length;
	}

	public void setLength(double length) {
		this.length = length;
	}

	public double getWidth() {
		return width;
	}

	public void setWidth(double width) {
		this.width = width;
	}

	public double getArea()
	{
		double area = length * width;
		return area;
	}
	
	public String toString()
	{
		return "The width is " + width + "\nThe length is " + length;
	}
}
