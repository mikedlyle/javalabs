package ex5;

import java.util.Scanner;

public class MonthDriver {

	public static void main(String[] args) 
	{
		Scanner kb = new Scanner(System.in);
		
		int month1;
		int month2;
		
		System.out.println("Enter a month number for 2 months,\nMonth 1: ");
		month1 = kb.nextInt();
		System.out.println("Month 2: ");
		month2 = kb.nextInt();
		
		Month object1 = new Month(month1);
		Month object2 = new Month(month2);
		
		//to string method
		System.out.println("Month 1: " + object1.toString());
		System.out.println("Month 2: " + object2.toString());
				
		//equals method, and greater than and less than methods
		System.out.println("True or false; Month 1 is equal to month 2: " + object1.equals(object2));
		System.out.println("True or false; Month 1 is greater than month 2: " + object1.greaterThan(object2));
		System.out.println("True or false; Month 1 is less than month 2: " + object1.lessThan(object2));

		kb.close();
	}

}
