package ex5;


public class Month
	{
		int monthNumber;
		
		public Month()
		{
			monthNumber = 1;
		}
		
		public Month(int monthNumber)
		{
			this.monthNumber = monthNumber;
			if(monthNumber < 1 || monthNumber > 12)
			{
				monthNumber = 1;
			}
		}
		
		public Month(String monthName)
		{
			
			switch(monthName) 
			{
			case "January": 
				monthNumber = 1;
				break;
			case "February": 
				monthNumber = 2;
				break;
			case "March": 
				monthNumber = 3;
				break;
			case "April": 
				monthNumber = 4;
				break;
			case "May": 
				monthNumber = 5;
				break;
			case "June": 
				monthNumber = 6;
				break;
			case "July": 
				monthNumber = 7;
				break;
			case "August": 
				monthNumber = 8;
				break;
			case "September": 
				monthNumber = 9;
				break;
			case "October": 
				monthNumber = 10;
				break;
			case "November": 
				monthNumber = 11;
				break;
			case "December": 
				monthNumber = 12;
				break;
			}
		}
		
		
		

		public void setMonthNumber(int monthNumber) 
		{
			this.monthNumber = monthNumber;
			if(monthNumber < 1 || monthNumber > 12)
			{
				monthNumber = 1;
			}
		}

		public int getMonthNumber() 
		{
			return monthNumber;
		}
		
		public String getMonthName()
		{
			String monthName = "";
			if(monthNumber == 1)
			{
				monthName = "January";
			}
			else if(monthNumber == 2)
			{
				monthName = "February";
			}
			else if(monthNumber == 3)
			{
				monthName = "March";
			}
			else if(monthNumber == 4)
			{
				monthName = "April";
			}
			else if(monthNumber == 5)
			{
				monthName = "May";
			}
			else if(monthNumber == 6)
			{
				monthName = "June";
			}
			else if(monthNumber == 7)
			{
				monthName = "July";
			}
			else if(monthNumber == 8)
			{
				monthName = "August";
			}
			else if(monthNumber == 9)
			{
				monthName = "September";
			}
			else if(monthNumber == 10)
			{
				monthName = "October";
			}
			else if(monthNumber == 11)
			{
				monthName = "November";
			}
			else if(monthNumber == 12)
			{
				monthName = "December";
			}
			else
			{
				monthName = "Not a valid month number";
			}
			
			return monthName;
		}
		
		public String toString()
		{
			return "Month Name: " + getMonthName();
		}
		
		public boolean equals(Month object2)
		{
			boolean status;
			
			if(this.getMonthName() == object2.getMonthName())
			{
				status = true;
			}
			else
			{
				status = false;
			}
			
			return status;
		}
		
		public boolean greaterThan(Month m2)
		{
			boolean status;
			if(this.monthNumber > m2.monthNumber)
			{
				status = true;
			}
			else
			{
				status = false;
			}
			return status;
		}
		
		public boolean lessThan(Month m2)
		{
			boolean status;
			if(this.monthNumber < m2.monthNumber)
			{
				status = true;
			}
			else
			{
				status = true;
			}
			return status;
		}
	}
