package ex4;
import java.util.Scanner;

public class LandTractDriver {

	public static void main(String[] args) {
		Scanner keyboard = new Scanner(System.in);
		
		double len1;
		double width1;
		
		double len2;
		double width2;
		
		System.out.println("What is the length of the first land tract?");
		len1 = keyboard.nextDouble();
		
		System.out.println("What is the width of the first land tract?");
		width1 = keyboard.nextDouble();
		
		System.out.println("What is the length of the second land tract?");
		len2 = keyboard.nextDouble();
		
		System.out.println("What is the width of the second land tract?");
		width2 = keyboard.nextDouble();
		
		LandTract object1 = new LandTract();
		LandTract object2 = new LandTract();
		
		object1.setLength(len1);
		object1.setWidth(width1);
		
		object2.setLength(len2);
		object2.setWidth(width2);
		
		if(object1.equals(object2)) 
		{
			System.out.println("The tracts are of equal size, and the area is " + object1.getArea());
		}
		else
		{
			System.out.println("The tracts are not of equal size.\nTract 1's area is " + object1.getArea() + 
					"\nTract 2's area is " + object2.getArea());
		}
		
		keyboard.close();
	}

}
