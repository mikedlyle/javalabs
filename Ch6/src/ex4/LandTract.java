package ex4;

public class LandTract {
	private double length;
	private double width;
	
	public double getLength() {
		return length;
	}
	public void setLength(double length) {
		this.length = length;
	}
	public double getWidth() {
		return width;
	}
	public void setWidth(double width) {
		this.width = width;
	}
	
	public double getArea()
	{
		return length * width;
	}
	
	public boolean equals(LandTract object2)
	{
		boolean status;
		
		if(this.getArea() == object2.getArea())
		{
			status = true;
		}
		else
		{
			status = false;
		}
		
		return status;
	}
	
	public String toString()
	{
		return "Length is " + length + ", width is " + width + " and area is " + getArea();
	}
}
