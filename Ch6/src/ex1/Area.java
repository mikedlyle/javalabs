package ex1;

public class Area {
	
	//returning area of circle
	public static double calcArea(double radius)
	{
		double area = Math.PI * (radius * radius);
		return area;
	}
	
	//returning area of rectangle
	public static double calcArea(double w,double l)
	{
		double area = w * l;
		return area;
	}
	
	//returning area of cylinder
	public static double calcArea(double radius,double height,double pi)
	{
		double area = Math.PI * (radius * radius) * height;
		return area;
	}
}
