package ex1;

public class AreaDriver {

	public static void main(String[] args) {
		System.out.println("Circle Area is: " + Area.calcArea(5));
		System.out.println("Rectangle Area is: " + Area.calcArea(5, 4));
		System.out.println("Cylinder Area is: " + Area.calcArea(2, 10, Math.PI));
	}
}
