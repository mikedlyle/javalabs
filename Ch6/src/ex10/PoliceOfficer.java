package ex10;

public class PoliceOfficer 
{
	private String officerName;
	private int badgeNum;
	
	//private ParkingMeter someMeter;
	
	//private ParkedCar someCar;
	
	//ParkedCar car1 = new ParkedCar();
	//wParkingTicket ticket1 = new ParkingTicket();
	
	public PoliceOfficer(String officerName, int badgeNum) 
	{
		this.officerName = officerName;
		this.badgeNum = badgeNum;
		//this.car1 = car1;
		//this.ticket1 = ticket1;
	}
	
	public PoliceOfficer()
	{
		
	}

	public String getOfficerName() {
		return officerName;
	}

	public void setOfficerName(String officerName) {
		this.officerName = officerName;
	}

	public int getBadgeNum() {
		return badgeNum;
	}

	public void setBadgeNum(int badgeNum) {
		this.badgeNum = badgeNum;
	}

	//public ParkedCar getCar1() {
	//	return car1;
	//}

	//public void setCar1(ParkedCar car1) {
	//	this.car1 = car1;
	//}

/*	public ParkingTicket getTicket1() {
		return ticket1;
	}

	public void setTicket1(ParkingTicket ticket1) {
		this.ticket1 = ticket1;
	}
	*/
	public ParkingTicket issueTicket(ParkedCar someCar, ParkingMeter someMeter)
	{
	//	this.someMeter = someMeter;
	//	this.someCar = someCar;
		
		//boolean ticketStatus;
		ParkingTicket aTicket=null;
		if(someMeter.getMinutesPurchased() < someCar.getNumMinutes())
		{
			int overMinutes = someCar.getNumMinutes()-someMeter.getMinutesPurchased();
			aTicket = new ParkingTicket(someCar,this);
			aTicket.getFine(overMinutes);
		}
		
		return aTicket;
	}
	
	public String toString()
	{
		return "Officer " + officerName + " has a badge number of " + badgeNum;
	}
	
}
