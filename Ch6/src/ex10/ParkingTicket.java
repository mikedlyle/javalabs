package ex10;

public class ParkingTicket 
{
//	ParkingTicket ticket1 = new ParkingTicket();
	
/*	public void setTicket1(ParkingTicket ticket1) {
		this.ticket1 = ticket1;
	}
*/
	//Parked Car fields
	private ParkedCar someCar;
	
	
	//Police officer fields
	private PoliceOfficer po;

	
	//Parking ticket fields
 private double fine;
	//private int hours;
	
	
	public ParkingTicket(ParkedCar someCar, PoliceOfficer po)
	{
		this.someCar = someCar;
		this.po = po;
	}
	

	
 /*
	public ParkingTicket getTicket1() {
		return ticket1;
	}

	public int getHours() {
		return hours;
	}

	public ParkedCar getMake() {
		return make;
	}

	public void setMake(ParkedCar make) {
		this.make = make;
	}

	public ParkedCar getModel() {
		return model;
	}

	public void setModel(ParkedCar model) {
		this.model = model;
	}

	public ParkedCar getColor() {
		return color;
	}

	public void setColor(ParkedCar color) {
		this.color = color;
	}

	public ParkedCar getLicenseNum() {
		return licenseNum;
	}

	public void setLicenseNum(ParkedCar licenseNum) {
		this.licenseNum = licenseNum;
	}

	public void setHours(int hours) {
		this.hours = hours;
	}

	//Police officer getters and setters
	public PoliceOfficer getOfficerName() {
		return officerName;
	}

	public void setOfficerName(PoliceOfficer officerName) {
		this.officerName = officerName;
	}

	public PoliceOfficer getBadgeNum() {
		return badgeNum;
	}

	public void setBadgeNum(PoliceOfficer badgeNum) {
		this.badgeNum = badgeNum;
	}

	public void setFine(double fine) {
		this.fine = fine;
	}
	*/

	//start of methods
	public double getFine(int overMinutes)
	{
		if(overMinutes <= 60)
		{
			fine =25;
		}
		else
		{
			int numHours = overMinutes / 60;
			fine = 25;
			numHours--;
			
			double remain = overMinutes % 60;
			if(remain != 0)
			{
				numHours++;
			}
			
			fine = fine + (numHours * 10);
		}
		
		return fine;
	}
	
	public String reportOfficer()
	{
		return "Officer Name: " + po.getOfficerName() + "\nBadge Number: " + po.getBadgeNum();
	}
	
	public String toString()
	{
		return "Make: " + someCar.getMake() + "\nModel: " + someCar.getModel() + "\nColor: " + someCar.getColor() + "\nLicense Number: " + someCar.getLicenseNumber() + " fine of " + fine;
	}
}
