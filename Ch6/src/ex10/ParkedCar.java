package ex10;

public class ParkedCar {
	private String make;
	private String model;
	private String color;
	private int licenseNumber;
	private int numMinutes;
	
	public ParkedCar(String make, String model, String color, int licenseNumber, int numMinutes) 
	{
		this.make = make;
		this.model = model;
		this.color = color;
		this.licenseNumber = licenseNumber;
		this.numMinutes = numMinutes;
	}
	
	public ParkedCar()
	{
		
	}
	
	public String getMake() {
		return make;
	}
	
	public void setMake(String make) {
		this.make = make;
	}
	
	public String getModel() {
		return model;
	}
	
	public void setModel(String model) {
		this.model = model;
	}
	
	public String getColor() {
		return color;
	}
	
	public void setColor(String color) {
		this.color = color;
	}
	
	public int getLicenseNumber() {
		return licenseNumber;
	}
	
	public void setLicenseNumber(int licenseNumber) {
		this.licenseNumber = licenseNumber;
	}
	
	public int getNumMinutes() {
		return numMinutes;
	}
	
	public void setNumMinutes(int numMinutes) {
		this.numMinutes = numMinutes;
	}
	
	
}
