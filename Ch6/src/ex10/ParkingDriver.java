package ex10;

public class ParkingDriver {

	public static void main(String[] args) 
	{
		ParkedCar car1 = new ParkedCar("Jeep", "Cherokie", "Red",78945,130);
		ParkingMeter meter1 = new ParkingMeter(10);
		
		PoliceOfficer goodCop = new PoliceOfficer("Winslow",15965);
		
		ParkingTicket t1 = goodCop.issueTicket(car1, meter1);
		
		if(t1 == null)
		{
			System.out.println("No ticket!");
		}
		else
		{
			System.out.println(goodCop.toString() + " issued a ticket for " + t1.toString());
		}
		
	}

}
