package ex6;

public class EmployeeDriver {

	public static void main(String[] args) 
	{	
		Employee ob1 = new Employee();
		Employee ob2 = new Employee("Lyle", 12345);
		Employee ob3 = new Employee("Mike D", 54321, "IT Department", "Developer");
		
		System.out.println(ob1.toString());
		System.out.println(ob2.toString());
		System.out.println(ob3.toString());
	}

}
