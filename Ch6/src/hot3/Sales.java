package hot3;
import java.io.*;
import java.util.Scanner;

public class Sales 
{
	public static void main(String[] args) throws IOException {
		PrintWriter weeklySales = new PrintWriter("WeeklySales.txt");
		
		Scanner keyboard = new Scanner(System.in);
	
		double sale = 0;
		double totalSales = 0;

	
		for(int i = 0; i < 5; i++)
		{
			System.out.println("Enter a sale.");
			sale = keyboard.nextDouble();
			if(sale > -1)
			{
				totalSales += sale;
				System.out.println("Sales: " + totalSales);
				weeklySales.println("Sales: " + totalSales);
			}
			else
			{
				System.out.println("Error: Enter a positive number");
				i--;
			}
		}
		
		System.out.printf("\n-------\nTotal Sale: $%.2f", totalSales);
		weeklySales.printf("\n-------\nTotal Sale: $%.2f", totalSales);
		System.out.println("\n\nYour output is printed in 'WeeklySales.txt'");
		weeklySales.close();
		keyboard.close();
	}
}
