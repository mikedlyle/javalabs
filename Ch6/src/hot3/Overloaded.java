package hot3;

public class Overloaded {

	private static int num1;
	private static double num2;
	private static int num3;
	
	public Overloaded()
	{
		
	}

	public static int getNum1() {
		return num1;
	}

	public static void setNum1(int num1) {
		Overloaded.num1 = num1;
	}

	public static double getNum2() {
		return num2;
	}

	public static void setNum2(double num2) {
		Overloaded.num2 = num2;
	}

	public static int getNum3() {
		return num3;
	}

	public static void setNum3(int num3) {
		Overloaded.num3 = num3;
	}
	
	public double addNums(int num1, double num2)
	{
		return num1 + num2;
	}
	
	public double addNums(int num1, double num2, int num3)
	{
		return num1 + num2 + num3;
	}
	
}
