package hot3;

public class Library {

	private Book book;
	private String name;
	
	public Library(Book book, String name) 
	{
		this.book = book;
		this.name = name;
	}

	public Book getBook() {
		return book;
	}

	public void setBook(Book book) {
		this.book = book;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}
	
	public String toString()
	{
		return book.toString();
	}
}
