package ex1;

public class TestScores
{
	private int[] scores;
	private int average;

	public TestScores(int[] scores)   throws IllegalArgumentException
	{    		
		average = returnAverage(scores);
		this.scores = scores;
	}
	
	private int returnAverage(int[] scores)
	{
		int avg = 0;
		for(int i =0; i < scores.length; i++)
		{
			if(scores[i] < 0 || scores[i] > 100)
				throw new IllegalArgumentException("Score for test "+ (i + 1) + " is not a valid score.");
			avg += scores[i];
		}
        return (avg / scores.length);
	}

	public int getAverage()
	{
		return average;
	}
}