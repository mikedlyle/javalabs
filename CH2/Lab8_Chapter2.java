import java.util.Scanner;

public class Lab8_Chapter2
{
    public static void main(String[] args)
    {
        double input;
        
        Scanner keyboard = new Scanner(System.in);
        System.out.println("Please enter the amount of your purchase.");
        input = keyboard.nextDouble();
        
        double stateTax = (input * 0.055);
        double countyTax = (input * 0.02);
        double totalTax = (stateTax + countyTax);
        double totalPrice = (input + totalTax);
        
        System.out.println("Your purchase is $" + input + ".\r\nState Tax: $" + stateTax + "\r\nCounty Tax: $" + countyTax + "\r\nTotal Tax: $" + 
                           totalTax + "\r\n------------------\r\nTotal Price: $" + totalPrice);
    }
}