public class Lab3_Chapter2
{
    public static void main(String[] args)
    {
        String firstName = "Mike";
        String address = "Boswell Ave, St Louis, MO 63114";
        String phoneNumber = "6364850922";
        String collegeMajor = "Web and Application Development";
        
        System.out.println(firstName + "\r\n" + address + "\r\n" + phoneNumber + "\r\n" + collegeMajor);
    }
}