import java.util.Scanner;

public class Lab12_Chapter2
{
    public static void main(String[] args)
    {
        String inputCity;
        
        Scanner keyboard = new Scanner(System.in);
        System.out.println("Please enter your favorite city.");
        inputCity = keyboard.nextLine();
        
        int numChars = inputCity.length();
        String cityUpper = inputCity.toUpperCase();
        String cityLower = inputCity.toLowerCase();
        char cityFirstChar = inputCity.charAt(0);
        
        System.out.println("Information about your favorite city\r\n-------------------\r\nNumber of characters: " + numChars + "\r\nUppercase City: " + 
                          cityUpper + "\r\nLowercase City: " + cityLower + "\r\nFirst character of city: " + cityFirstChar);
    }
}