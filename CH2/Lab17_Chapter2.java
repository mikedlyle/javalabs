import java.util.Scanner;

public class Lab17_Chapter2
{
    public static void main(String[] args)
    {
        String name;
        int age;
        String city;
        String college;
        String profession;
        String animal;
        String petName;
        
        Scanner keyboard = new Scanner(System.in);
        System.out.println("Please enter your name.");
        name = keyboard.nextLine();
        
        System.out.println("Please enter your age.");
        age = keyboard.nextInt();
                
        keyboard.nextLine();
        
        System.out.println("Please enter the name of a city.");
        city = keyboard.nextLine();
        
        System.out.println("Please enter the name of a college.");
        college = keyboard.nextLine();
        
        System.out.println("Please enter a profession.");
        profession = keyboard.nextLine();
        
        System.out.println("Please enter a type of animal.");
        animal = keyboard.nextLine();
        
        System.out.println("Please enter a pets name.");
        petName = keyboard.nextLine();
        
        System.out.println("There once was a person named " + name + " who lived in " + city + ". At the age of " + age + ", " + name + 
                           " went to college at " + college + ". " + name + " graduated and went to work as a " + profession + ". Then, " + 
                          name + " adopted a " + animal + " named " + petName + ". They both lived happily ever after.");
    }
}