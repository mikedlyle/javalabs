public class Lab2_Chapter2
{
    public static void main(String[] args)
    {
        String firstName = "Mike";
        String middleName = "David";
        String lastName = "Lyle";
        
        String firstInitial = "M";
        String middleInitial = "D";
        String lastInitial = "L";
        
        System.out.println("My first name is " + firstName + 
                          ". My middle name is " + middleName + 
                          ". My last name is " + lastName + 
                          ".\r\nMy first initial is " + firstInitial + 
                          ". My middle initial is " + middleInitial + 
                          ". My last initial is " + lastInitial + ".");
    }
}