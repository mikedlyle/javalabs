import java.util.Scanner;

public class Lab13_Chapter2
{
    public static void main(String[] args)
    {
        double input;
        
        Scanner keyboard = new Scanner(System.in);
        System.out.println("Please enter your restaurant bill.");
        input = keyboard.nextDouble();
        
        double tax = (input * 0.075);
        double tip = (input * 0.18);
        double total = (input + tax + tip);
        
        System.out.println("\r\nMeal Charge: $" + input);
        System.out.println("Tax: $" + tax);
        System.out.println("Tip: $" + tip);
        System.out.println("--------------------");
        System.out.println("Total: $" + total);
    }
}