public class Lab1_Chapter2
{
    public static void main(String[] args)
    {
        String name = "Lyle";
        int age = 26;
        double annualPay = 50000;
        
        System.out.println("My name is " + name + ", my age is " + age + " and I hope to earn " + annualPay + " per year.");
    }
}