public class Lab16_Chapter2
{
    public static void main(String[] args)
    {
        System.out.println("A soft drink company surveyed 15,000 customers. Approx 18 percent purchase one or more energy drinks per week." + 
                          "/r/nOf those who purchase energy drinks, approx 58 percent prefer citrus.");

        double custTotal = 15000;
        double custEnergyDrinks = custTotal * 0.18;
        double custCitrus = custEnergyDrinks * 0.58;
        
        System.out.println("\nAppoximate number of customers who purchase one or more energy drinks per week is:: " + custEnergyDrinks);
        System.out.println("\nAppoximate number of customers who prefer citrus flavored energy drinks is:: " + custCitrus);
    }
}