import java.util.Scanner;

public class Lab11_Chapter2
{
    public static void main(String[] args)
    {
        double inputMales;
        double inputFemales;
        
        Scanner keyboard = new Scanner(System.in);
        System.out.println("Please enter the amount of males registered to your class.");
        inputMales = keyboard.nextDouble();
        
        System.out.println("Please enter the amount of females registered to your class.");
        inputFemales = keyboard.nextDouble();
        
        double totalStudents = (inputFemales + inputMales);
        double malePercentage = (inputMales / totalStudents);
        double femalePercentage = (inputFemales / totalStudents);
        
        System.out.println("\r\nMales: " + inputMales + "\r\nFemales: " + inputFemales + "\r\nTotal Students: " + totalStudents + 
                           "\r\nPercentage of Males: " + malePercentage + "%\r\nPercentage of Females: " + femalePercentage + 
                          "%");
    }
}