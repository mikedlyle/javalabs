import java.util.Scanner;

public class Lab9_Chapter2
{
    public static void main(String[] args)
    {
        double inputMiles;
        double inputGallons;
        
        Scanner keyboard = new Scanner(System.in);
        System.out.println("Please enter the miles driven.");
        inputMiles = keyboard.nextDouble();
        
        System.out.println("Please enter the gallons of gas used.");
        inputGallons = keyboard.nextDouble();
        
        double mpg = (inputMiles / inputGallons);
        
        System.out.println("\r\nYour total MPG for this trip is: " + mpg);
    }
}