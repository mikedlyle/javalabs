public class Lab7_Chapter2
{
    public static void main(String[] args)
    {
        double oneAcre = 43560;
        double acresToCalc = 389767;
        
        System.out.println("If the total number of square feet is 389767, then the total acres would be " + (acresToCalc / oneAcre));
    }
}