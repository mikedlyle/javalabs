import java.util.Scanner;


public class Lab5_Chapter2
{
    public static void main(String[] args)
    {
        int input;
        
        int bagOfCookies = 40;
        int cookieCalories = 75;
        int servingSize = 300;
        
        Scanner keyboard = new Scanner(System.in);
        System.out.println("Please enter the number of cookies you have eaten.");
        input = keyboard.nextInt();
        System.out.println("You have eaten " + input + " cookies. This amounts to " + (cookieCalories * input) + " total calories consumed.");
    }
}