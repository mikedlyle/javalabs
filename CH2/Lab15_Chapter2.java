import java.util.Scanner;

public class Lab15_Chapter2
{
    public static void main(String[] args)
    {
        double inputCookies; //the number of cookies the user inputs
        
        double sugar = 1.5;
        double butter = 1;
        double flour = 2.75;
        double numCookies = 48; //the number of cookies made with the above ingredients
        
        Scanner keyboard = new Scanner(System.in);
        System.out.println("Please enter the amount of cookies you would like to make.");
        inputCookies = keyboard.nextDouble();
        
        sugar = (sugar / numCookies) * inputCookies;
        butter = (butter / numCookies) * inputCookies;
        flour = (flour / numCookies) * inputCookies;
        
        
        System.out.println("\r\n:::To make " + inputCookies + " cookies, you will need the following ingredients:::");
        System.out.println("--------------------");
        System.out.println("Sugar: " + sugar);
        System.out.println("Butter: " + butter);
        System.out.println("Flour: " + flour);
    }
}