import java.util.Scanner;

public class Lab10_Chapter2
{
    public static void main(String[] args)
    {
        double inputTest1;
        double inputTest2;
        double inputTest3;
        
        Scanner keyboard = new Scanner(System.in);
        System.out.println("Please enter your first test score.");
        inputTest1 = keyboard.nextDouble();
        
        System.out.println("Please enter your second test score.");
        inputTest2 = keyboard.nextDouble();
        
        System.out.println("Please enter your third test score.");
        inputTest3 = keyboard.nextDouble();
        
        double totalScore = ((inputTest1 + inputTest2 + inputTest3) / 3);
        
        System.out.println("\r\nThe average of your tests is: " + totalScore);
    }
}