public class Lab14_Chapter2
{
    public static void main(String[] args)
    {
        System.out.println("Kathryn bought 1000 shares of a stock at $25.50 per share. \nShe has to pay her broker 2 percent.\nHere are the details of her order.");

        double numShares = 1000;
        double sharePrice = 25.50;
        double commission = (numShares * sharePrice) * 0.02;
        double total = (numShares * sharePrice) + commission;
        
        System.out.printf("\nAmount paid for stock: %,.2f\n",(numShares * sharePrice));
        System.out.printf("Amount paid for commission: %,.2f\n",commission);
        System.out.printf("Total amount paid: %,.2f\n",total);
    }
}