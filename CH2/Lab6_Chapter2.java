public class Lab6_Chapter2
{
    public static void main(String[] args)
    {
        double totalSales = 8300000;
        double percentSales = 0.65;
        
        System.out.println("If the total number of sales this year is " + totalSales + ". Then the total this company will generate is " + (totalSales * percentSales) + ".");
    }
}