package ex2;

public class CarDriver {

	public static void main(String[] args) 
	{
		Car car1 = new Car(2018, "Jeep");
		
		//getting initial speed value
		System.out.println(car1.getSpeed());
		
		//calling acceleration method to speed up car object
		
		//first acceleration
		car1.accelerate();
		System.out.println(car1.getSpeed());
		//second acceleration
		car1.accelerate();
		System.out.println(car1.getSpeed());
		//third acceleration
		car1.accelerate();
		System.out.println(car1.getSpeed());
		//fourth acceleration
		car1.accelerate();
		System.out.println(car1.getSpeed());
		//fifth acceleration
		car1.accelerate();
		System.out.println(car1.getSpeed());
		
		//calling the brake method to slow car object down
		
		//first brake
		car1.brake();
		System.out.println(car1.getSpeed());
		//second brake
		car1.brake();
		System.out.println(car1.getSpeed());
		//third brake
		car1.brake();
		System.out.println(car1.getSpeed());
		//fourth brake
		car1.brake();
		System.out.println(car1.getSpeed());
		//fifth brake
		car1.brake();
		System.out.println(car1.getSpeed());
	}

}
