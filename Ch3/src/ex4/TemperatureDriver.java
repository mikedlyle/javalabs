package ex4;

import java.util.Scanner;

public class TemperatureDriver {

	public static void main(String[] args) {
		double tempInput;
		
		Scanner keyboard = new Scanner(System.in);
		
		System.out.println("Enter a temperature in fahrenheit to convert.");
		tempInput = keyboard.nextDouble();
		
		Temperature temp1 = new Temperature(tempInput);
		
		temp1.setFahrenheit(tempInput);
		
		System.out.println("Your temperature in fahrenheit is: " + temp1.getFahrenheit());
		System.out.println("Your temperature in celsius is: " + temp1.getCelsius());
		System.out.println("Your temperature in kelvin is: " + temp1.getKelvin());
		
		keyboard.close();
	}
}
