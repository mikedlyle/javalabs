package ex9;

public class Circle 
{
	private double radius;
	private final double PI = 3.14159;
	
	public Circle(double radius)
	{
		this.radius = radius;
	}
	
	public void setRadius(double radius)
	{
		this.radius = radius;
	}
	
	public double getRadius()
	{
		return radius;
	}
	
	public double getArea()
	{
		double area = PI * radius * radius;
		return area;
	}
	
	public double getDiameter()
	{
		double diameter = radius * 2.0;
		return diameter;
	}
	
	public double getCircumference()
	{
		double circumference = 2.0 * PI * radius;
		return circumference;
	}
}









