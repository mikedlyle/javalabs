package ex9;
import java.util.Scanner;

public class CircleDriver {

	public static void main(String[] args) {
		
		Scanner keyboard = new Scanner(System.in);
		
		double radius;
		
		System.out.println("Enter a circles radius: ");
		radius = keyboard.nextDouble();
		
		Circle circle1 = new Circle(radius);
		
		System.out.println("Radius: " + circle1.getRadius());
		System.out.println("Area: " + circle1.getArea());
		System.out.println("Diameter: " + circle1.getDiameter());
		System.out.println("Circumference: " + circle1.getCircumference());
		
		keyboard.close();
	}

}
