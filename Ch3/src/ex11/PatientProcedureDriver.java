package ex11;

public class PatientProcedureDriver {

	public static void main(String[] args) {
		Patient patient1 = new Patient("Mike", "David", "Lyle", "3644 Boswell Ave", "St John", "MO", "63114", "6364850922", "Morgan", "3143241988");

		Procedure procedure1 = new Procedure("Physical Exam", "1/22/2018", "Dr. Irvine", 250.00);
		Procedure procedure2 = new Procedure("X-Ray", "1/22/2018", "Dr. Jamison", 500.00);
		Procedure procedure3 = new Procedure("Blood Test", "1/22/2018", "Dr. Smith", 200.00);
		
		System.out.println("Patient Information\n------------------------");
		System.out.println("Name: " + patient1.getFirstName() + " " + patient1.getMiddleName() + " " + patient1.getLastName());
		System.out.println("Address: " + patient1.getAddress() + ", " + patient1.getCity() + ", " + patient1.getState() + ", " + patient1.getZipCode());
		System.out.println("Phone Number: " + patient1.getPhoneNumber());
		System.out.println("\nEmergency Contact Information: \n------------------------");
		System.out.println("Name: " + patient1.getEmergencyName());
		System.out.println("Phone Number: " + patient1.getEmergencyPhone());
		
		System.out.println("\n\n\nProcedure Information: ");
		
		System.out.println("\nProcedure #1: \n------------------------");
		System.out.println("Procedure name: \n" + procedure1.getProcedureName());
		System.out.println("Date: " + procedure1.getProcedureDate());
		System.out.println("Practitioner: " + procedure1.getPractitioner());
		System.out.printf("Charge: $%.2f%n", procedure1.getCharges());
		
		System.out.println("\nProcedure #2: \n------------------------");
		System.out.println("Procedure name: \n" + procedure2.getProcedureName());
		System.out.println("Date: " + procedure2.getProcedureDate());
		System.out.println("Practitioner: " + procedure2.getPractitioner());
		System.out.printf("Charge: $%.2f%n", procedure2.getCharges());
		
		System.out.println("\nProcedure #3: \n------------------------");
		System.out.println("Procedure name: \n" + procedure3.getProcedureName());
		System.out.println("Date: " + procedure3.getProcedureDate());
		System.out.println("Practitioner: " + procedure3.getPractitioner());
		System.out.printf("Charge: $%.2f%n", procedure3.getCharges());
	}

}
