package ex6;

public class Payroll 
{
	private String employeeName;
	private int idNum;
	private double hourlyPay;
	private double numHours;
	
	public Payroll(double hourlyPay, double numHours)
	{
		this.hourlyPay = hourlyPay;
	}
	
	public void setEmployeeName(String employeeName)
	{
		this.employeeName = employeeName;
	}
	
	public void setIdNum(int idNum)
	{
		this.idNum = idNum;
	}

	public String getEmployeeName()
	{
		return employeeName;
	}
	
	public int IdNum()
	{
		return idNum;
	}
	
	public double getHourlyPay()
	{
		return hourlyPay;
	}
	
	public double getNumHours()
	{
		return numHours;
	}
	

	
	public double calcGrossPay()
	{
		 return hourlyPay * numHours;
		
	}
}
