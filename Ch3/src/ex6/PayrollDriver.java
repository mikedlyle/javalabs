package ex6;
import java.util.Scanner;

public class PayrollDriver {

	public static void main(String[] args) {
		
		String name;
		int idNum;
		double hourlyPay;
		double numHours;
		
		System.out.println("Enter the following data for the employee you are looking for: \n\n\nName: ");
		Scanner keyboard = new Scanner(System.in);
		name = keyboard.nextLine();
		
		System.out.println("ID Number: ");
		idNum = keyboard.nextInt();
		
		System.out.println("Hourly Pay: ");
		hourlyPay = keyboard.nextDouble();
		
		System.out.println("Hours worked: ");
		numHours = keyboard.nextDouble();
		
		Payroll emp1 = new Payroll(hourlyPay,numHours);
		
		emp1.setEmployeeName(name);
		emp1.setIdNum(idNum);
		
		System.out.println("Info for " + name + "\n--------------------");
		System.out.println("Hourly Pay: " + emp1.getHourlyPay());
		System.out.println("Hours Worked: " + emp1.getNumHours());
		System.out.println("Gross Pay: " + emp1.calcGrossPay());
		keyboard.close();
	}

}
