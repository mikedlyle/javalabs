package Ex1;

public class EmployeeDriver {

	public static void main(String[] args) 
	{	
		Employee e1 = new Employee();
		Employee e2 = new Employee();
		Employee e3 = new Employee();
		
		e1.setName("Susan Meyers");
		e1.setId(47899);
		e1.setDepartment("Accounting");
		e1.setPosition("Vice President");
		
		e2.setName("Mark Jones");
		e2.setId(39119);
		e2.setDepartment("IT");
		e2.setPosition("Programmer");
		
		e3.setName("Joy Rogers");
		e3.setId(81774);
		e3.setDepartment("Manufacturing");
		e3.setPosition("Engineer");
		
		System.out.println(e1.getName() + "\n" + e1.getId() + "\n" + e1.getDepartment() + "\n" + e1.getPosition());
		System.out.println("\n" + e2.getName() + "\n" + e2.getId() + "\n" + e2.getDepartment() + "\n" + e2.getPosition());
		System.out.println("\n" + e3.getName() + "\n" + e3.getId() + "\n" + e3.getDepartment() + "\n" + e3.getPosition());

	}

}
