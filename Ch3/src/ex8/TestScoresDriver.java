package ex8;
import java.util.Scanner;
public class TestScoresDriver {

	public static void main(String[] args) {
		TestScores testScores1 = new TestScores();
		
		Scanner keyboard = new Scanner(System.in);
		
		double test1;
		double test2;
		double test3;
		
		System.out.println("Enter 3 test scores to find the average: \n\nTest 1: ");
		test1 = keyboard.nextDouble();
		
		System.out.println("Test 2: ");
		test2 = keyboard.nextDouble();
		
		System.out.println("Test 3: ");
		test3 = keyboard.nextDouble();
		
		
		testScores1.setTest1(test1);
		testScores1.setTest2(test2);
		testScores1.setTest3(test3);
		
		System.out.printf("Average: %.2f%%%n",testScores1.returnAverage());
		keyboard.close();
	}

}
