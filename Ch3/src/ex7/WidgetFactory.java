package ex7;

public class WidgetFactory {
	private double numWidgets;
	private double numDays;
	
	public WidgetFactory(double numWidgets)
	{
		this.numWidgets = numWidgets;
	}
	
	public double dayToProduce()
	{
		numDays = (numWidgets / 160);
		return numDays;
	}
}
