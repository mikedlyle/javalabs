package ex7;

public class WidgetFactoryDriver {

	public static void main(String[] args) {
		WidgetFactory widget1 = new WidgetFactory(1000);
		
		System.out.println("Total number of days needed to produce is: " + widget1.dayToProduce());
	}

}
