
public class Student 
{	
	//Instance field with a private access modifier
	private int studentId;
	private String studentName;
	private double studentGPA;
	
	//mutator method or setter
	public void setId(int id)
	{
		studentId = id;
	}
	
	//accessor method or getter
	public int getId()
	{
		return studentId;
	}
	
	public void setName(String name) 
	{
		studentName = name;
	}
	
	public String getName()
	{
		return studentName;
	}
	
	public void setGPA(double gpa)
	{
		if(gpa > 4.0)
		{
			studentGPA = 0.0;
		}
		else
		{
			studentGPA = gpa;
		}
	}
	
	public double getGPA()
	{
		return studentGPA;
	}
}













