package ex3;

public class PersonalInformation 
{
	private String name;
	private String address;
	private int age;
	private int phoneNumber;
	
	public PersonalInformation()
	{
		
	}
	
	public void setName(String name)
	{
		this.name = name;
	}
	
	public void setAddress(String address)
	{
		this.address = address;
	}
	
	public void setAge(int age)
	{
		this.age = age;
	}
	
	public void setPhoneNumber(int phoneNumber)
	{
		this.phoneNumber = phoneNumber;
	}
	
	public String getName()
	{
		return name;
	}
	
	public String getAddress()
	{
		return address;
	}
	
	public int getAge()
	{
		return age;
	}
	
	public int getPhoneNumber()
	{
		return phoneNumber;
	}
}











