package ex3;

public class PersonalInformationDriver {

	public static void main(String[] args) {
		PersonalInformation person1 = new PersonalInformation();
		PersonalInformation person2 = new PersonalInformation();
		PersonalInformation person3 = new PersonalInformation();
		
		person1.setName("Lyle");
		person1.setAddress("Boswell, St Louis MO");
		person1.setAge(26);
		person1.setPhoneNumber(4850922);
		
		person2.setName("Steven");
		person2.setAddress("South St Louis, MO");
		person2.setAge(21);
		person2.setPhoneNumber(5553636);
		
		person3.setName("Stich");
		person3.setAddress("South County, MO");
		person3.setAge(20);
		person3.setPhoneNumber(4589632);
		
		System.out.println("Person 1 Information: ");
		System.out.println(person1.getName());
		System.out.println(person1.getAddress());
		System.out.println(person1.getAge());
		System.out.println(person1.getPhoneNumber());
		
		System.out.println("\nPerson 2 Information: ");
		System.out.println(person2.getName());
		System.out.println(person2.getAddress());
		System.out.println(person2.getAge());
		System.out.println(person2.getPhoneNumber());
		
		System.out.println("\nPerson 3 Information: ");
		System.out.println(person3.getName());
		System.out.println(person3.getAddress());
		System.out.println(person3.getAge());
		System.out.println(person3.getPhoneNumber());
		
	}

}
