package ex10;
import java.util.Scanner;

public class PetDriver {

	public static void main(String[] args) {
		
		Scanner keyboard = new Scanner(System.in);
		
		String name;
		String type;
		int age;
		
		System.out.println("Enter the following information about your pet: \n\nName of pet: ");
		name = keyboard.nextLine();
		
		System.out.println("Type of pet: ");
		type = keyboard.nextLine();
		
		System.out.println("Age of pet: ");
		age = keyboard.nextInt();
		
		Pet pet1 = new Pet();

		pet1.setName(name);
		pet1.setType(type);
		pet1.setAge(age);
		
		System.out.println("Name: " + pet1.getName());
		System.out.println("Type: " + pet1.getType());
		System.out.println("Age: " + pet1.getAge());
		keyboard.close();
	}

}
