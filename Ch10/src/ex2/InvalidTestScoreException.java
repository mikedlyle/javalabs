package ex2;

public class InvalidTestScoreException extends Exception {
	
	public InvalidTestScoreException()
	{
		super("Invalid test score.");
	}
	
}
