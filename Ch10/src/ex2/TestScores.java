package ex2;

public class TestScores
{
	private int[] scores;
	private int average;

	public TestScores(int[] scores)   throws InvalidTestScoreException
	{    		
		average = returnAverage(scores);
		this.scores = scores;
	}
	
	public int getAverage()
	{
		return average;
	}
	
	private int returnAverage(int[] scores)
	{
		int avg = 0;
		
		for(int i =0; i < scores.length; i++)
		{
			if(scores[i] < 0 || scores[i] > 100)
			{
				try 
				{
					throw new InvalidTestScoreException();
				}
				catch(InvalidTestScoreException a)
				{
					System.out.println("Invalid test score");
				}
				//throw new InvalidTestScoreException("Score for test "+ (i + 1) + " is not a valid score.");	
			}
			
			avg += scores[i];
		}
        return (avg / scores.length);
	}
	
}