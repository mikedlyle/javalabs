package ex1;
import java.util.Scanner;

public class TestScoresDriver
{
	public static void main(String[] args)
	{
		int[] scoresArray = getArray(getInput("How many tests would you like to enter?"));
		try
		{
			TestScores scores = new TestScores(scoresArray);
			System.out.println("\nAverage score: " + scores.getAverage());
		}
		catch (IllegalArgumentException e)
		{
			System.out.println("You have entered an invalid score." + e.getMessage());
		}
	}

	public static int[] getArray(int n)
	{
		int[] scores = new int[n];
		for (int i = 0; i < scores.length; i++)
		{
			scores[i] = getInput("Enter score for test #" + (i + 1));
		}
		return scores;
	}

	public static int getInput(String s)
	{
		Scanner keyboard = new Scanner(System.in);
		System.out.println(s);
		return keyboard.nextInt();
	}
}