package ex2;
import java.util.Scanner;
public class PayrollDriver {

	public static void main(String[] args) {
		
		Scanner keyboard = new Scanner(System.in);
		
		int[] hours = new int[7];
		double[] payRate = new double[7];
		
		Payroll p1 = new Payroll();
		
		for(int i = 0; i < 7; ++i)
		{
			do {
				System.out.println("Enter hours for employee: " + p1.getEmployeeId(i));
				hours[i] = keyboard.nextInt();
			}while(hours[i] < 0);
			
			do {
				System.out.println("Enter pay rate for employee" + p1.getEmployeeId(i));
				payRate[i] = keyboard.nextInt();
			}while(payRate[i] < 6.00);
		}
		
		p1.setHours(hours);
		p1.setRate(payRate);
		
		System.out.println(p1);
		
		System.out.println("Enter ID# for gross pay: ");
		int id = keyboard.nextInt();
		
		double grossPay = p1.getEmployeeGrossPay(id);
		
		if(grossPay == -1)
		{
			System.out.println("Employee does not exist");
		}
		else
		{
			System.out.printf("Employee %d has gross pay of: $%,.2f", id, grossPay);
		}
		
		keyboard.close();
	}

}
