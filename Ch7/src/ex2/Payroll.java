package ex2;

public class Payroll {

	int[] employeeId = {5658845,4520125,7895122,8777541,8451277,1302850,7580489};
	int[] hours = new int[7];
	double[] payRate = new double[7];
	double[] wages = new double[7];
	
	public Payroll()
	{
		
	}
	
	private void calculateWage()
	{
		for(int i = 0; i < employeeId.length; ++i)
		{
			wages[i] = payRate[i] * hours[i];
		}
	}
	
	public void setRate(double[] payRate)
	{
		for(int i = 0; i < employeeId.length; ++i)
		{
			this.payRate[i] = payRate[i];
		}
		
		calculateWage();
	}
	
	public void setHours(int[] hoursWorked)
	{
		for(int i = 0; i < employeeId.length; ++i)
		{
			this.hours[i] = hoursWorked[i];
		}
		
		calculateWage();
	}
	
	public int getEmployeeId(int index)
	{
		return employeeId[index];
	}
	
	public double getEmployeeGrossPay(int empId)
	{
		double wage = -1;
		for(int i = 0; i < employeeId.length; ++i)
		{
			if(employeeId[i] == empId)
			{
				wage = this.wages[i];
			}
		}
		return wage;
	}
	

	public String toString()
	{
		String output = "";
		for(int i = 0; i < employeeId.length; ++i)
		{
			output += "Employee# : ";
		}
		return output;
	}
	
}
