package ex1;
import java.util.Scanner;

public class Rainfall
{
	double[] totalRainFall = new double[12];
	
	public Rainfall(double[] rain)
	{
		for (int i = 0; i < rain.length; i++)
		{
			totalRainFall[i] = rain[i];
		}
	}
	
	public double calcRainForYear(double[] totalRainFall)
	{
		double total = 0;
		
		for (int i = 0; i < totalRainFall.length; i++)
		{
			total += totalRainFall[i];
		}
		return total;
	}
	
	public double calcAverageMonthlyRain(double[] totalRainFall)
	{
		double total = 0;
		double average;
		
		for (int i = 0; i < totalRainFall.length; i++)
		{
			total += totalRainFall[i];
		}
		average = total / totalRainFall.length;
		return average;
	}
	
	public double calcMostRain(double[] totalRainFall)
	{
		double highest = totalRainFall[0];
		
		for (int i = 1; i < totalRainFall.length; i++)
		{
			if (totalRainFall[i] > highest)
			{
				highest = totalRainFall[i];
			}
		}
		return highest;
	}
	
	public double calcLeastRain(double[] totalRainFall)
	{
		double lowest = totalRainFall[0];
		
		for (int i = 1; i < totalRainFall.length; i++)
		{
			if (totalRainFall[i] < lowest)
			{
				lowest = totalRainFall[i];
			}
		}
		return lowest;
	}
}

