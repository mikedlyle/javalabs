package ex1;
import java.util.Scanner;

public class RainFallDriver {

	public static void main(String[] args) {
		Scanner keyboard = new Scanner(System.in);
		
		double input = 0;
		double[] totalRainFall = new double[12];
		
		Rainfall rain = new Rainfall(totalRainFall);
		
		
		for (int i = 0; i < totalRainFall.length; i++)
		{
			do
			{
				System.out.println("Enter total rainfall for month " + (i + 1));
				totalRainFall[i] = keyboard.nextDouble();
			}
			while(totalRainFall[i] < 0);
		}
		
		System.out.println("Average: " + rain.calcAverageMonthlyRain(totalRainFall));
		System.out.println("Least Rainfall: " + rain.calcLeastRain(totalRainFall));
		System.out.println("Most Rainfall: " + rain.calcMostRain(totalRainFall));
		System.out.println("Total: " + rain.calcRainForYear(totalRainFall));
		
		keyboard.close();
	}

}
