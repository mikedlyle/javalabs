package ex2;

import java.lang.String;
import java.lang.Exception;
import javafx.application.Application;
import javafx.stage.Stage;
import javafx.scene.Scene;
import javafx.scene.layout.VBox;
import javafx.scene.control.Label;
import javafx.scene.control.Button;
import javafx.scene.control.TextField;
import javafx.event.EventHandler;
import javafx.event.ActionEvent;

public class NameFormatter extends Application{
	TextField firstName = new TextField();
	TextField middleName = new TextField();
	TextField lastName = new TextField();
	TextField prefTitle = new TextField();
	
	Button button1 = new Button();
	Button button2 = new Button();
	Button button3 = new Button();
	Button button4 = new Button();
	Button button5 = new Button();
	Button button6 = new Button();
	
	Label fLabel;
	Label mLabel;
	Label lLabel;
	Label tLabel;
	
	String first;
	String middle;
	String last;
	String title;
	
	public static void main(String[] args) {
		launch(args);

	}

	@Override
	public void start(Stage primaryStage) throws Exception {
		primaryStage.setTitle("NameFormatter");
		
		button1 = new Button("CLICK ME");
		button2 = new Button("CLICK ME");
		button3 = new Button("CLICK ME");
		button4 = new Button("CLICK ME");
		button5 = new Button("CLICK ME");
		button6 = new Button("CLICK ME");
		
		Label fLabel = new Label("First Name: ");
		Label mLabel = new Label("Middle Name: ");
		Label lLabel = new Label("Last Name: ");
		Label tLabel = new Label("Preferred Title: ");
		
		VBox vb1 = new VBox(fLabel, firstName, mLabel, middleName, lLabel, lastName, tLabel, prefTitle, button1, button2, button3, button4, button5, button6);
		
		Scene scene1 = new Scene(vb1, 500, 500);
		
		button1.setOnAction(new Button1ClickHandler());
		button2.setOnAction(new Button2ClickHandler());
		button3.setOnAction(new Button3ClickHandler());
		button4.setOnAction(new Button4ClickHandler());
		button5.setOnAction(new Button5ClickHandler());
		button6.setOnAction(new Button6ClickHandler());
		
		primaryStage.setScene(scene1);
	
		primaryStage.show();
	}

	class Button1ClickHandler implements EventHandler<ActionEvent>
	{
		@Override
		public void handle(ActionEvent event)
		{
			title = prefTitle.getText();
			first = firstName.getText();
			middle = middleName.getText();
			last = lastName.getText();
			
			button1.setText(title + ", " + first + " " + middle + " "  + last);
		}
	}
	
	class Button2ClickHandler implements EventHandler<ActionEvent>
	{
		@Override
		public void handle(ActionEvent event)
		{
			first = firstName.getText();
			middle = middleName.getText();
			last = lastName.getText();
			
			button2.setText(first + " " + middle + " " + last);
		}
	}
	
	class Button3ClickHandler implements EventHandler<ActionEvent>
	{
		@Override
		public void handle(ActionEvent event)
		{
			first = firstName.getText();
			last = lastName.getText();
			
			button3.setText(first + " " + last);
		}
	}
	
	class Button4ClickHandler implements EventHandler<ActionEvent>
	{
		@Override
		public void handle(ActionEvent event)
		{
			title = prefTitle.getText();
			first = firstName.getText();
			middle = middleName.getText();
			last = lastName.getText();
			
			button4.setText(last + ", " + first + " " + middle + ", " + title);
		}
	}
	
	class Button5ClickHandler implements EventHandler<ActionEvent>
	{
		@Override
		public void handle(ActionEvent event)
		{
			first = firstName.getText();
			middle = middleName.getText();
			last = lastName.getText();
			
			button5.setText(last + ", " + first + " " + middle);
		}
	}
	
	class Button6ClickHandler implements EventHandler<ActionEvent>
	{
		@Override
		public void handle(ActionEvent event)
		{
			first = firstName.getText();
			last = lastName.getText();
			
			button6.setText(last + ", " + first);
		}
	}
}
