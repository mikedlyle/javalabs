package ex1;

import javafx.application.Application;
import javafx.stage.Stage;
import javafx.scene.Scene;
import javafx.scene.layout.VBox;
import javafx.scene.control.Label;
import javafx.scene.control.Button;
import javafx.event.EventHandler;
import javafx.geometry.Pos;
import javafx.event.ActionEvent;

public class LatinTranslator extends Application {
	Label mainLabel = new Label();
	
	public static void main(String[] args) {
		launch(args);
	}

	@Override
	public void start(Stage primaryStage) throws Exception {
		//SETTING THE STAGE TITLE
		primaryStage.setTitle("Latin Translator");
		
		Button sinisterButton = new Button("Sinister");
		Button dexterButton = new Button("Dexter");
		Button mediumButton = new Button("Medium");
		mainLabel = new Label("");
		
		VBox vbox1 = new VBox(40, sinisterButton, dexterButton, mediumButton, mainLabel);
		
		Scene scene = new Scene(vbox1, 250, 250);
		vbox1.setAlignment(Pos.TOP_CENTER);
		primaryStage.setScene(scene);
		
		//SHOW THE WINDOW
		primaryStage.show();
		
		sinisterButton.setOnAction(new SinisterClickHandler());
		dexterButton.setOnAction(new DexterClickHandler());
		mediumButton.setOnAction(new MediumClickHandler());
	}
	
	class SinisterClickHandler implements EventHandler<ActionEvent>
	{
		@Override
		public void handle(ActionEvent event)
		{
			mainLabel.setText("Left");
		}
	}
	
	class DexterClickHandler implements EventHandler<ActionEvent>
	{
		@Override
		public void handle(ActionEvent event)
		{
			mainLabel.setText("Right");
		}
	}
	
	class MediumClickHandler implements EventHandler<ActionEvent>
	{
		@Override
		public void handle(ActionEvent event)
		{
			mainLabel.setText("Center");
		}
	}

	
}