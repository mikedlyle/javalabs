package ex3;

import java.lang.String;
import java.text.DecimalFormat;
import java.text.NumberFormat;
import java.lang.Exception;
import javafx.application.Application;
import javafx.stage.Stage;
import javafx.scene.Scene;
import javafx.scene.layout.VBox;
import javafx.scene.control.Label;
import javafx.scene.control.Button;
import javafx.scene.control.TextField;
import javafx.event.EventHandler;
import javafx.event.ActionEvent;
public class TipTaxTotal extends Application {

	TextField totalInput = new TextField();
	
	Button button = new Button();
	
	Label totalLbl = new Label();
	//18% tip
	Label tipLbl = new Label();
	//7% tax
	Label taxLbl = new Label();
	Label totalCost = new Label();
	
	Label tipLabel = new Label();
	Label taxLabel = new Label();
	Label totalLabel  = new Label();
	double total;
	double tip;
	double tax;
	
	public static void main(String[] args) {
		launch(args);
	}

	@Override
	public void start(Stage primaryStage) throws Exception {
		primaryStage.setTitle("Tip Tax Total");
		
		Label totalCost = new Label("Total Price: ");
		Label tipLabel = new Label("Tip: ");
		Label taxLabel = new Label("Tax: ");
		Label totalLabel = new Label("Total: ");
		
		button = new Button("Calculate");
		
		VBox vb1 = new VBox(totalCost, totalInput, button, tipLabel, tipLbl, taxLabel, taxLbl, totalLabel, totalLbl);
		
		Scene scene1 = new Scene(vb1, 500, 500);
		
		button.setOnAction(new ButtonClickHandler());
		
		primaryStage.setScene(scene1);
		
		primaryStage.show();
	}
	
	//Integer.parseInt(enter variable here);
	
	class ButtonClickHandler implements EventHandler<ActionEvent>
	{
		@Override
		public void handle(ActionEvent event)
		{
			DecimalFormat df = new DecimalFormat("$0.00");
			total = Double.parseDouble(totalInput.getText());
			//totalDouble = Double.parseDouble(total);
			tip = total * 0.07;
			tax = total * 0.18;
			
			tipLbl.setText(df.format(tip));
			taxLbl.setText(df.format(tax));
			totalLbl.setText(df.format(total + tax + tip));
		}
	}

}
