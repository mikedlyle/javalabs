package ex7;

public class FatGram {
	private double calories;
	private double fatGrams;
	
	public FatGram(double calories, double fatGrams)
	{
		this.calories = calories;
		this.fatGrams = fatGrams;
	}
	
	public double percentageFatCalories()
	{
		double caloriesFromFat = fatGrams * 9.0;
		
		if(caloriesFromFat < (calories * 0.3))
		{
			System.out.println("Food is low in fat.");
		}
		else if(caloriesFromFat > calories)
		{
			System.out.println("Error: Calories from fat cannot be greater than calories.");
		}
		
		return (caloriesFromFat / calories) * 100;
	}
}
