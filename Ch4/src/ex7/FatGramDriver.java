package ex7;
import java.util.Scanner;
public class FatGramDriver {

	public static void main(String[] args) {
		Scanner keyboard = new Scanner(System.in);
		
		double calories;
		double fatGrams;
		
		System.out.println("Enter total number of calories");
		calories = keyboard.nextDouble();
		
		System.out.println("Enter total grams of fat");
		fatGrams = keyboard.nextDouble();
		
		FatGram food1 = new FatGram(calories, fatGrams);
		
		System.out.println("Calories: " + calories);
		System.out.println("Fat Grams: " + fatGrams);
		System.out.printf("%.2f%% calories from fat.",food1.percentageFatCalories());
		
		keyboard.close();
	}

}
