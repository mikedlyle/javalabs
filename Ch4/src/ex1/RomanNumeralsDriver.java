package ex1;
import java.util.Scanner;
public class RomanNumeralsDriver {

	public static void main(String[] args) {
		int input;
		
		Scanner keyboard = new Scanner(System.in);
		System.out.println("Enter a number 1 through 10 to get roman numeral value.");
		input = keyboard.nextInt();
		
		if(input == 1)
		{
			System.out.println("I");
		}
		else if(input == 2)
		{
			System.out.println("II");
		}
		else if(input == 3)
		{
			System.out.println("III");
		}
		else if(input == 4)
		{
			System.out.println("IV");
		}
		else if(input == 5)
		{
			System.out.println("V");
		}
		else if(input == 6)
		{
			System.out.println("VI");
		}
		else if(input == 7)
		{
			System.out.println("VII");
		}
		else if(input == 8)
		{
			System.out.println("VIII");
		}
		else if(input == 9)
		{
			System.out.println("IX");
		}
		else if(input == 10)
		{
			System.out.println("X");
		}
		else {
			System.out.println("Error: Number must be 1-10");
		}
		
		
		keyboard.close();
	}

}
