package hot_ch4;

public class Lawn {
	private double width;
	private double length;

	public Lawn()
	{
		
	}

	public Lawn(double width, double length) 
	{
		this.width = width;
		this.length = length;
	}

	public double getWidth() {
		return width;
	}

	public void setWidth(double width) {
		this.width = width;
	}

	public double getLength() {
		return length;
	}

	public void setLength(double length) {
		this.length = length;
	}
	
	public double returnSqFeet(double width, double length)
	{
		this.width = width;
		this.length = length;
		double sqFeet = width * length;
		
		return sqFeet;
	}
	
	
	
	
	
}












