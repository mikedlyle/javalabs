package hot_ch4;
import java.util.Scanner;

public class LawnDriver {

	public static void main(String[] args) {
		Scanner keyboard = new Scanner(System.in);
		
		double length;
		double width;
		double fee1;
		
		System.out.println("Enter the following information(in feet) about your first lawn to return the fees for"
				+ " a 20 week season.\n\nLength: ");
		length = keyboard.nextDouble();
		
		System.out.println("Width: ");
		width = keyboard.nextDouble();
		
		Lawn lawn1 = new Lawn(width, length);
		
		if(lawn1.returnSqFeet(width, length) < 400)
		{
			fee1 = 25;
			System.out.printf("Your total for a 20 week season would be $%.2f", (fee1 * 20));
		}
		else if(lawn1.returnSqFeet(width, length) > 400 && lawn1.returnSqFeet(width, length) < 600)
		{
			fee1 = 35;
			System.out.printf("Your total for a 20 week season would be $%.2f", (fee1 * 20));
		}
		else if(lawn1.returnSqFeet(width, length) >= 600)
		{
			fee1 = 50;
			System.out.printf("Your total for a 20 week season would be $%.2f", (fee1 * 20));
		}
		else {
			fee1 = 0;
			System.out.printf("Your total for a 20 week season would be $%.2f", (fee1 * 20));
		}
		
		double length2 = 0;
		double width2;
		double fee2;
		
		Lawn lawn2 = new Lawn();
		
		System.out.println("\n\n\nEnter the following information(in feet) about your second lawn to return the fees "
				+ "for a 20 week season.\n\nLength: ");
		length2 = keyboard.nextDouble();
		
		System.out.println("Width: ");
		width2 = keyboard.nextDouble();
		
		lawn2.setLength(length2);
		lawn2.setWidth(width2);
		
		if(lawn2.returnSqFeet(width2, length2) <= 400)
		{
			fee2 = 25;
			System.out.printf("Your total for a 20 week season would be $%.2f", (fee2 * 20));
		}
		else if(lawn2.returnSqFeet(width2, length2) >= 400 && lawn2.returnSqFeet(width2, length2) < 600)
		{
			fee2 = 35;
			System.out.printf("Your total for a 20 week season would be $%.2f", (fee2 * 20));
		}
		else if(lawn2.returnSqFeet(width2, length2) >= 600)
		{
			fee2 = 50;
			System.out.printf("Your total for a 20 week season would be $%.2f", (fee2 * 20));
		}
		else 
		{
			fee2 = 0;
			System.out.printf("Your total for a 20 week season would be $%.2f", (fee2 * 20));
		}
		System.out.printf("\n\nFee for lawn 1 = $%.2f", (fee1 * 20));
		System.out.printf("\nFee for lawn 2 = $%.2f", (fee2 * 20));
		
		keyboard.close();
	}
}










