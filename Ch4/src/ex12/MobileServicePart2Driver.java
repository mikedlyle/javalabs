package ex12;

import java.util.Scanner;

public class MobileServicePart2Driver {

	public static void main(String[] args) {
		Scanner keyboard = new Scanner(System.in);
		
		String pickPackage;
		double minutes;
		
		System.out.println("Enter a, b, or c to choose a mobile package.");
		pickPackage = keyboard.nextLine();
		
		System.out.println("Enter minutes used.");
		minutes = keyboard.nextDouble();
		
		MobileServicePart2 bill1 = new MobileServicePart2();
		
		bill1.setSubscription(pickPackage);
		
		bill1.setMinutes(minutes);
		
		System.out.printf("Total Monthly Bill: $%.2f", bill1.calcMonthlyBill());
		
		//////////////////////////////////////////////////////////////////////////////////////////////////////////////////
		
		if(pickPackage.equalsIgnoreCase("a"))
		{
			if(bill1.calcPackageA() > bill1.calcPackageB())
			{
				System.out.printf("\nYou could save $%.2f if you chose package B", (bill1.calcPackageA() - bill1.calcPackageB()));
			}
			
			if(bill1.calcPackageA() > bill1.calcPackageC())
			{
				System.out.printf("\nYou could save $%.2f if you chose package C", (bill1.calcPackageA() - bill1.calcPackageC()));
			}
		}
		
		if(pickPackage.equalsIgnoreCase("b"))
		{
			if(bill1.calcPackageB() > bill1.calcPackageC())
			{
				System.out.printf("\nYou could save $%.2f if you chose package C", (bill1.calcPackageB() - bill1.calcPackageC()));
			}
		}
		
		keyboard.close();
	}

}
























