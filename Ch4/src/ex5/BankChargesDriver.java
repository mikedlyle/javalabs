package ex5;
import java.util.Scanner;

public class BankChargesDriver {

	public static void main(String[] args) {
		Scanner keyboard = new Scanner(System.in);
		
		double balance;
		double checksWritten;
		
		System.out.println("Enter account balance: ");
		balance = keyboard.nextDouble();
		
		System.out.println("Enter number of checks written: ");
		checksWritten = keyboard.nextDouble();
		
		BankCharges account1 = new BankCharges(checksWritten, balance);
		
		System.out.printf("Starting Balance: $%,.2f",balance);
		System.out.printf("\nNumber of Checks Written: %.0f",checksWritten);
		System.out.printf("\nService Fees: $%,.2f", account1.serviceFees());
		System.out.printf("\n-----------\nEnding Balance: $%,.2f", (balance - account1.serviceFees()));
		keyboard.close();
	}

}
