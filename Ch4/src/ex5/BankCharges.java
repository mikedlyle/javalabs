package ex5;

public class BankCharges 
{
	private double numberChecks;
	private double endingBalance;
	
	public BankCharges(double numberChecks, double endingBalance)
	{
		this.numberChecks = numberChecks;
		this.endingBalance = endingBalance;
	}
	
	public double serviceFees()
	{
		double serviceFee = 10.0;
		if(numberChecks < 20.0)
		{
			serviceFee += numberChecks * 0.1;
		}
		else if(numberChecks >= 20.0 && numberChecks <= 39.0)
		{
			serviceFee += numberChecks * 0.08;
		}
		else if(numberChecks >= 40.0 && numberChecks <= 59.0)
		{
			serviceFee += numberChecks * 0.06;
		}
		else
		{
			serviceFee += numberChecks * 0.04;
		}
		
		if(endingBalance < 400.0)
		{
			serviceFee += 15.0;
		}
		
		return serviceFee;
	}
}













