package ex8;

public class RunningTheRaceDriver {

	public static void main(String[] args) {
		
		
		RunningTheRace runner1 = new RunningTheRace("Lyle", "Stich", "Steven", 10, 11, 12);
		
		System.out.println(runner1.getFirstPlace());
		System.out.println(runner1.getSecondPlace());
		System.out.println(runner1.getThirdPlace());
	}

}
