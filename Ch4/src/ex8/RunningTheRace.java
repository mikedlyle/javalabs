package ex8;

public class RunningTheRace {
	private String runner1;
	private String runner2;
	private String runner3;
	private double time1;
	private double time2;
	private double time3;
	
	public RunningTheRace(String runner1, String runner2, String runner3, double time1, double time2, double time3)
	{
		this.runner1 = runner1;
		this.runner2 = runner2;
		this.runner3 = runner3;
		this.time1 = time1;
		this.time2 = time2;
		this.time3 = time3;
	}
	
	
	
	public String getRunner1() {
		return runner1;
	}



	public void setRunner1(String runner1) {
		this.runner1 = runner1;
	}



	public String getRunner2() {
		return runner2;
	}



	public void setRunner2(String runner2) {
		this.runner2 = runner2;
	}



	public String getRunner3() {
		return runner3;
	}



	public void setRunner3(String runner3) {
		this.runner3 = runner3;
	}



	public double getTime1() {
		return time1;
	}



	public void setTime1(double time1) {
		this.time1 = time1;
	}



	public double getTime2() {
		return time2;
	}



	public void setTime2(double time2) {
		this.time2 = time2;
	}



	public double getTime3() {
		return time3;
	}



	public void setTime3(double time3) {
		this.time3 = time3;
	}

	public String getFirstPlace()
	{
		String winner;
		if(time1 < time2 && time1 < time3)
		{
			winner = String.format("%s is the winner", runner1);
		}
		else if(time2 < time1 && time2 < time3)
		{
			winner = String.format("%s is the winner", runner2);
		}
		else {
			winner = String.format("%s is the winner", runner3);
		}
		return winner;
	}
	
	public String getSecondPlace()
	{
		String secondPlace;
		if((time1 > time2 && time1 < time3) || (time1 < time2 && time1 > time3))
		{
			secondPlace = String.format("%s got second place", runner1);
		}
		else if((time2 > time1 && time2 < time3) || (time2 < time1 && time2 > time3))
		{
			secondPlace = String.format("%s got second place", runner2);
		}
		else {
			secondPlace = String.format("%s got second place", runner3);
		}
		return secondPlace;
	}

	public String getThirdPlace()
	{
		String loser;
		if(time1 > time2 && time1 > time3)
		{
			loser = String.format("%s is the loser", runner1);
		}
		else if(time2 > time1 && time2 > time3)
		{
			loser = String.format("%s is the loser", runner2);
		}
		else {
			loser = String.format("%s is the loser", runner3);
		}
		return loser;
	}
}
