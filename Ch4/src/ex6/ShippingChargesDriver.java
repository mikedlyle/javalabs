package ex6;
import java.util.Scanner;
public class ShippingChargesDriver {

	public static void main(String[] args) {
		Scanner keyboard = new Scanner(System.in);
		
		
		
		ShippingCharges charge1 = new ShippingCharges(2,550);
		
		System.out.printf("Total shipping charge: $%.2f", charge1.calcShippingCharges());
		keyboard.close();
	}

}
