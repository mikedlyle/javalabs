package ex6;

public class ShippingCharges {
	private double packageWeight;
	private double miles;
	
	public ShippingCharges(double packageWeight, double miles)
	{
		this.packageWeight = packageWeight;
		this.miles = miles;
	}
	
	
	
	public double calcShippingCharges()
	{
		double rate;
		
		if(packageWeight <= 2)
		{
			rate = 1.10;
		}
		else if(packageWeight <= 6)
		{
			rate = 2.20;
		}
		else if(packageWeight <= 10)
		{
			rate = 3.70;
		}
		else
		{
			rate = 4.80;
		}
		
		int counter = (int)miles / 500;
		
		if(miles % 500 != 0)
		{
			counter++;
		}
		return rate * counter;
	}
	
}
