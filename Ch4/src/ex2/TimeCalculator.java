package ex2;
import java.util.Scanner;

public class TimeCalculator {

	public static void main(String[] args) {
		
		double seconds;
		
		Scanner keyboard = new Scanner(System.in);
		
		System.out.println("Enter a number of seconds: ");
		seconds = keyboard.nextDouble();
		
		if(seconds >= 60)
		{
			seconds = seconds / 60;
			System.out.printf("%.2f minutes",seconds);
		}
		else if(seconds >= 3600)
		{
			seconds = seconds / 3600;
			System.out.printf("%.2f hours",seconds);
		}
		else if(seconds >= 86400)
		{
			seconds = seconds / 86400;
			System.out.printf("%.2f days",seconds);
		}
		else
		{
			System.out.printf("%.2f seconds",seconds);
		}
		
		keyboard.close();
	}

}
