package ex10;

public class FreezingAndBoiling {

	private double temperature;
	private boolean tempBool;
	public double getTemperature() {
		return temperature;
	}

	public void setTemperature(double temperature) {
		this.temperature = temperature;
	}
	
	public boolean isEthylFreezing()
	{
		if(temperature <= -173)
		{
			tempBool = true;
		}else
		{
			tempBool = false;
		}
		return tempBool;
	}
	
	public boolean isEthylBoiling()
	{
		if(temperature >= 172)
		{
			tempBool = true;
		}else
		{
			tempBool = false;
		}
		return tempBool;
	}
	
	public boolean isOxygenFreezing()
	{
		if(temperature <= -362)
		{
			tempBool = true;
		}else
		{
			tempBool = false;
		}
		return tempBool;
	}
	
	public boolean isOxygenBoiling()
	{
		if(temperature >= -306)
		{
			tempBool = true;
		}else
		{
			tempBool = false;
		}
		return tempBool;
	}
	
	public boolean isWaterFreezing()
	{
		if(temperature <= 32)
		{
			tempBool = true;
		}else
		{
			tempBool = false;
		}
		return tempBool;
	}
	
	public boolean isWaterBoiling()
	{
		if(temperature >= 212)
		{
			tempBool = true;
		}else
		{
			tempBool = false;
		}
		return tempBool;
	}
}
