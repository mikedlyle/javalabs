package ex10;
import java.util.Scanner;

public class FreezingAndBoilingDriver {

	public static void main(String[] args) {
		Scanner keyboard = new Scanner(System.in);
		
		double tempInput;
		
		FreezingAndBoiling temp1 = new FreezingAndBoiling();
		
		System.out.println("Enter a temperature in Fahrenheit to determine freezing and boiling points.");
		tempInput = keyboard.nextDouble();
		
		temp1.setTemperature(tempInput);
		
		if(temp1.isEthylFreezing() == true)
		{
			System.out.println("Ethyl will freeze at " + tempInput);
		}else if(temp1.isEthylBoiling() == true)
		{
			System.out.println("Ethyl will boil at " + tempInput);
		}
		
		if(temp1.isOxygenFreezing() == true)
		{
			System.out.println("Oxygen will freeze at " + tempInput);
		}else if(temp1.isOxygenBoiling() == true)
		{
			System.out.println("Oxygen will boil at " + tempInput);
		}
		
		if(temp1.isWaterFreezing() == true)
		{
			System.out.println("Water will freeze at " + tempInput);
		}else if(temp1.isWaterBoiling() == true)
		{
			System.out.println("Water will boil at " + tempInput);
		}
		
		
		keyboard.close();
	}

}
