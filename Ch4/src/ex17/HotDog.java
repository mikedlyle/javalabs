package ex17;

public class HotDog {
	/* Assume that hot dogs come in packages of 10, and hot dog buns come in packages of 8. Write a program 
	that calculates the number of packages of hot dogs and the number of packages of hot dog buns needed 
	for a cookout, with the minimum amount of leftovers. The program should ask the user for the number 
	of people attending the cookout, and the number of hot dogs each person will be given. The program 
	should display: 
	●The minimum number of packages of hot dogs required 
	●The minimum number of packages of buns required 
	●The number of hot dogs that will be left over 
	●The number of buns that will be left over */
	
	
	private int hotDogs = 10;
	private int hotDogBuns = 8;
	private int people;
	
	public HotDog(final int hotDogs, final int hotDogBuns, int people)
	{
		this.hotDogs = hotDogs;
		this.hotDogBuns = hotDogBuns;
		this.people = people;
	}

	public int getPeople() {
		return people;
	}

	public void setPeople(int people) {
		this.people = people;
	}

	public int getHotDogs() {
		return hotDogs;
	}

	public void setHotDogs(int hotDogs) {
		this.hotDogs = hotDogs;
	}

	public int getHotDogBuns() {
		return hotDogBuns;
	}

	public void setHotDogBuns(int hotDogBuns) {
		this.hotDogBuns = hotDogBuns;
	}
	
	public int packageHotdogs()
	{
		int packages = 0;
		int packageBunLeftover = 0;
	
		if(hotDogs = 8)
			
	}
}
