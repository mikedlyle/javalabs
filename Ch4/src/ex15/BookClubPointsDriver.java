package ex15;
import java.util.Scanner;
public class BookClubPointsDriver {

	public static void main(String[] args) {
		Scanner keyboard = new Scanner(System.in);
		
		int books;
		System.out.println("How many books have you purchased?");
		books = keyboard.nextInt();
		
		BookClubPoints book1 = new BookClubPoints(books);

		System.out.println("Books: " + books + "\nPoints: " + book1.pointsAwarded());
		
		keyboard.close();
	}
}
