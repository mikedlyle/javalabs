package ex15;

public class BookClubPoints {
	private int books;
	
	public BookClubPoints(int books)
	{
		this.books = books;
	}
	
	public int pointsAwarded()
	{
		int points;
		
		if(books <= 0)
		{
			points = 0;
		}else if(books == 1)
		{
			points = 5;
		}else if(books == 2)
		{
			points = 15;
		}else if(books == 3)
		{
			points = 30;
		}else {
			points = 60;
		}
		
		return points;
	}
}
