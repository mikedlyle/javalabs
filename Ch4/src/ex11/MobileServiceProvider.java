package ex11;

public class MobileServiceProvider {
	private String subscription;
	private double minutes;
	

	
	public String getSubscription() {
		return subscription;
	}

	public void setSubscription(String subscription) {
		this.subscription = subscription;
	}

	public double getMinutes() {
		return minutes;
	}

	public void setMinutes(double minutes) {
		this.minutes = minutes;
	}

	public double calcMonthlyBill()
	{
		double monthlyBill = 0.0;
		
		if(subscription.equalsIgnoreCase("a"))
		{
			if(minutes <= 450)
			{
				monthlyBill = 39.99;
			}
			else {

				monthlyBill = 0.45 * (minutes - 450) + 39.99;
			}
		}else if(subscription.equalsIgnoreCase("b")) 
		{
			if(minutes <= 900)
			{
				monthlyBill = 59.99;
			}
			else {

				monthlyBill = 0.40 * (minutes - 900) + 59.99;
			}
		}
		else if(subscription.equalsIgnoreCase("c"))
		{
			monthlyBill = 69.99;
		}
		
		return monthlyBill;
	}
}
