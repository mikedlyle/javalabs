package ex11;
import java.util.Scanner;

public class MobileServiceProviderDriver {

	public static void main(String[] args) {
		Scanner keyboard = new Scanner(System.in);
		
		String pickPackage;
		double minutes;
		
		System.out.println("Enter a, b, or c to choose a mobile package.");
		pickPackage = keyboard.nextLine();
		
		System.out.println("Enter minutes used.");
		minutes = keyboard.nextDouble();
		
		MobileServiceProvider bill1 = new MobileServiceProvider();
		
		bill1.setSubscription(pickPackage);
		
		bill1.setMinutes(minutes);
		
		System.out.printf("Total Monthly Bill: $%.2f", bill1.calcMonthlyBill());
		
		
		keyboard.close();
	}

}
