package ex13;
import java.util.Scanner;

public class BodyMassIndexDriver {

	public static void main(String[] args) {
		
		Scanner keyboard = new Scanner(System.in);
		
		double inches;
		double weight;
		
		System.out.println("Enter your height in inches");
		inches = keyboard.nextDouble();
		
		System.out.println("Enter your weight in lbs");
		weight = keyboard.nextDouble();
		
		BodyMassIndex bmi = new BodyMassIndex(weight, inches);
		
		double bmi1 = weight * (703 / (inches * inches));
		
		System.out.printf("BMI: %.2f\n",bmi1);		
		System.out.println(bmi.bmiResult());
		
		keyboard.close();
	}

}
