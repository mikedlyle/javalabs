package ex13;

public class BodyMassIndex {
	private double weight;
	private double height;
	
	public BodyMassIndex(double weight, double inches)
	{
		this.weight = weight;
		this.height = inches;
	}
	
	public void setWeight(double weight)
	{
		this.weight = weight;
	}
	
	public void setHeight(double height)
	{
		this.height = height;
	}
	
	public double getWeight()
	{
		return weight;
	}
	
	public double getHeight()
	{
		return height;
	}
	
	public String bmiResult()
	{
		double bmi = weight * (703 / (height * height));
		String result;
		if(bmi > 25)
		{
			result = "You are overweight.";
		}else if(bmi < 18.5)
		{
			result = "You are underweight.";
		}else {
			result = "You are at the optimal weight.";
		}
		return result;
	}
}










