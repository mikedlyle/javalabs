package ex9;

public class SpeedOfSound {

	private double distance;

	public double getDistance() {
		return distance;
	}

	public void setDistance(double distance) {
		this.distance = distance;
	}
	
	public double getSpeedInAir()
	{
		double time = distance / 1100;
		
		return time;
	}
	
	public double getSpeedInWater()
	{
		double time = distance / 4900;

		return time;
	}
	
	public double getSpeedInSteel()
	{
		double time = distance / 16400;
		
		return time;
	}
}
