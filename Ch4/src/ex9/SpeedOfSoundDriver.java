package ex9;
import java.util.Scanner;

public class SpeedOfSoundDriver {

	public static void main(String[] args) {

		String userInput;
		double distance;
		
		Scanner keyboard = new Scanner(System.in);
		
		SpeedOfSound speed1 = new SpeedOfSound();
		
		System.out.println("Enter 'air', 'water', or 'steel'");
		userInput = keyboard.nextLine();

		System.out.println("Enter a distance");
		distance = keyboard.nextDouble();
		
		speed1.setDistance(distance);
		
		if(userInput.equalsIgnoreCase("air"))
		{
			System.out.println(speed1.getSpeedInAir() + " seconds");
		}else if(userInput.equalsIgnoreCase("water"))
		{
			System.out.println(speed1.getSpeedInWater() + " seconds");
		}else if(userInput.equalsIgnoreCase("steel"))
		{
			System.out.println(speed1.getSpeedInSteel() + " seconds");
		}
		
		
		
		keyboard.close();
	}

}
