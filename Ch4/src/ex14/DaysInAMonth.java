package ex14;

public class DaysInAMonth {

	private int month;
	private int year;

	public DaysInAMonth(int month, int year)

	{
		this.month = month;
		this.year = year;
	}

	public int getMonth()
	{
		return month;
	}


	public int getYear()

	{
		return year;
	}


	public int getDays()
	{
		int days = 0;

		switch(month)
		{	
		case 1:
			days = 31;
			break;
		case 2:
			if (year % 100 == 0)
			{
				if (year % 400 == 0)
				{
					days = 29;
				}
			}

			else if (year % 4 == 0)
			{
				days = 29;
			}
			else
			{
				days = 28;
			}
			break;
		case 3:
			days = 31;
			break;
		case 4:
			days = 30;
			break;
		case 5:
			days = 31;
			break;
		case 6:
			days = 30;
			break;
		case 7:
			days = 31;
			break;
		case 8:
			days = 31;
			break;
		case 9:
			days = 30;
			break;
		case 10:
			days = 31;
			break;
		case 11:
			days = 30;
			break;
		case 12:
			days = 31;
			break;
		}
		return days;
	}
}