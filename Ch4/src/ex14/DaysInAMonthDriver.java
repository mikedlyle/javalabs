package ex14;

import java.util.Scanner;



public class DaysInAMonthDriver 
{
	public static void main(String[] args) 
	{
		int monthInput;
		int yearInput;
		
		Scanner keyboard = new Scanner(System.in);
		
		System.out.println("Enter a month (1-12):");
		monthInput = keyboard.nextInt();
		
		System.out.println("Enter a year:");
		yearInput = keyboard.nextInt();
		
		DaysInAMonth md1 = new DaysInAMonth(monthInput, yearInput);
		
		System.out.println(md1.getDays() + " days");
		
		keyboard.close();
	}
}