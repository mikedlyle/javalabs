package ex16;
import java.util.Scanner;

public class MagicDatesDriver {

	public static void main(String[] args) {
		Scanner keyboard = new Scanner(System.in);
		
		int month;
		int day;
		int year;
		
		System.out.println("Enter a month");
		month = keyboard.nextInt();
		
		System.out.println("Enter a day of that month");
		day = keyboard.nextInt();
		
		System.out.println("Enter a 2 digit year");
		year = keyboard.nextInt();
		
		MagicDates date1 = new MagicDates(month, day, year);
		
		System.out.println("Date is Magic: " + date1.isMagic());
		
		keyboard.close();
	}

}
