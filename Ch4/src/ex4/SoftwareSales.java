package ex4;

public class SoftwareSales 
{
	private double unitsSold;
	
	public SoftwareSales(double unitsSold)
	{
		this.unitsSold = unitsSold;
	}
	
	public double totalCost()
	{
		double totalCost = unitsSold * 99;
		if(unitsSold >= 100)
		{
			totalCost = totalCost * 0.5;
		}
		else if(unitsSold >= 50)
		{
			totalCost = totalCost * 0.4;
		}
		else if(unitsSold >= 20)
		{
			totalCost = totalCost * 0.3;
		}
		else if(unitsSold >= 10)
		{
			totalCost = totalCost * 0.2;
		}
		
		return totalCost;
	}
}
