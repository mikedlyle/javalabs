package ex4;
import java.util.Scanner;
public class SoftwareSalesDriver {

	public static void main(String[] args) {
		Scanner keyboard = new Scanner(System.in);
		
		double input;
		System.out.println("Enter the number of packages you would like to purchase");
		input = keyboard.nextDouble();
		
		SoftwareSales sale1 = new SoftwareSales(input);
		
		System.out.printf("Total Cost: $%.2f%n",sale1.totalCost());
	
		
		keyboard.close();
	}

}
