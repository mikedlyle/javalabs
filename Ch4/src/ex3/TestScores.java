package ex3;

public class TestScores {
	private double test1;
	private double test2;
	private double test3;
	private double average;
	
	public TestScores(double test1, double test2, double test3) 
	{
		this.test1 = test1;
		this.test2 = test2;
		this.test3 = test3;
	}
	
	public double getTest1()
	{
		return test1;
	}
	
	public double getTest2()
	{
		return test2;
	}
	
	public double getTest3()
	{
		return test3;
	}
	
	public double returnAverage()
	{
		average = (test1 + test2 + test3) / 3;
		return average;
	}
	
	public String returnLetter()
	{
		String letterGrade = null;
		if(average >= 90 && average <= 100)
		{
			letterGrade = "A";
		}
		else if(average >= 80)
		{
			letterGrade = "B";
		}
		else if(average >= 70)
		{
			letterGrade = "C";
		}
		else if(average >= 60)
		{
			letterGrade = "D";
		}
		else if(average >= 1 && average < 60) 
		{
			letterGrade = "F";
		}
		else {
			letterGrade = "Average must be between 1-100";
		}
		return letterGrade;
	}
}










