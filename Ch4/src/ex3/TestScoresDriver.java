package ex3;
import java.util.Scanner;

public class TestScoresDriver {

	public static void main(String[] args) {
		Scanner keyboard = new Scanner(System.in);
		
		System.out.println("Enter your test scores to get an average");
		
		double test1;
		double test2;
		double test3;
		
		System.out.println("Enter 3 test scores to find the average: \n\nTest 1: ");
		test1 = keyboard.nextDouble();
		
		System.out.println("Test 2: ");
		test2 = keyboard.nextDouble();
		
		System.out.println("Test 3: ");
		test3 = keyboard.nextDouble();
		
		TestScores testScores1 =  new TestScores(test1, test2, test3);
		
		System.out.printf("Average: %.2f%%%n",testScores1.returnAverage());
		System.out.println("\nLetter Grade: " + testScores1.returnLetter());
		keyboard.close();

	}

}
