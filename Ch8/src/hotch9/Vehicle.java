package hotch9;

public class Vehicle {
	protected int speed = 0;

	public Vehicle(int speed) {
		this.speed = speed;
	}
	
	public Vehicle()
	{
		
	}

	public int getSpeed() {
		return speed;
	}

	public void setSpeed(int speed) {
		this.speed = speed;
	}

	public int Accelerate() 
	{
		speed += 5;
		return speed;
	}
}
