package hotch9;

public class VehicleDriver {

	public static void main(String[] args) {
		
		Vehicle car1 = new Car();
		Vehicle truck1 = new Truck();
		
		System.out.println("CAR SPEED");
		System.out.println(car1.getSpeed());
		car1.Accelerate();
		System.out.println(car1.getSpeed());
		car1.Accelerate();
		System.out.println(car1.getSpeed());
		car1.Accelerate();
		System.out.println(car1.getSpeed());
		
		System.out.println("\nTRUCK SPEED");
		System.out.println(truck1.getSpeed());
		truck1.Accelerate();
		System.out.println(truck1.getSpeed());
		truck1.Accelerate();
		System.out.println(truck1.getSpeed());
		truck1.Accelerate();
		System.out.println(truck1.getSpeed());
		
		
	}

}
