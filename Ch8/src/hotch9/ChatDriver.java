package hotch9;
import java.util.Scanner;

public class ChatDriver {

	public static void main(String[] args) {
		
		//creating keyboard object for user input
		Scanner keyboard = new Scanner(System.in);

		//creating arrays
		double[] areaCode = {262, 414, 608, 715, 815, 920};
		double[] perMinuteRate = {.07, .10, .05, .16, .24, .14};
		
		//fields
		double code;
		double minutes;
		double totalCost;
		
		//asking for user input
		System.out.println("Please enter an area code: ");
		code = keyboard.nextDouble();
		
		System.out.println("Please enter call in minutes: ");
		minutes = keyboard.nextDouble();
		
		//math to determine total cost and statements to determine if area code entered equals an area code of the array
		if(code == areaCode[0])
		{
			totalCost = minutes * perMinuteRate[0];
			System.out.printf("Total cost is: $%.2f",totalCost);
		}
		else if(code == areaCode[1])
		{
			totalCost = minutes * perMinuteRate[1];
			System.out.printf("Total cost is: $%.2f",totalCost);
		}
		else if(code == areaCode[2])
		{
			totalCost = minutes * perMinuteRate[2];
			System.out.printf("Total cost is: $%.2f",totalCost);
		}
		else if(code == areaCode[3])
		{
			totalCost = minutes * perMinuteRate[3];
			System.out.printf("Total cost is: $%.2f",totalCost);
		}
		else if(code == areaCode[4])
		{
			totalCost = minutes * perMinuteRate[4];
			System.out.printf("Total cost is: $%.2f",totalCost);
		}
		else if(code == areaCode[5])
		{
			totalCost = minutes * perMinuteRate[5];
			System.out.printf("Total cost is: $%.2f",totalCost);
		}
		else
		{
			System.out.println("Invalid area code.");
		}
		
		//closing the keyboard
		keyboard.close();
	}

}
