package hotch9;
import java.util.Scanner;

public class GameDriver {

	public static void main(String[] args) {
		//creating scanner object for user input
		Scanner keyboard = new Scanner(System.in);
		
		//fields for game
		String gameName;
		int numPlayers;
		int timeLimit;
		
		//asking input from user to instantiate objects
		System.out.println("Enter the name of the Game.");
		gameName = keyboard.nextLine();
		
		System.out.println("Enter the max number of players.");
		numPlayers = keyboard.nextInt();
		
		System.out.println("Enter the time limit of the Game in minutes.");
		timeLimit = keyboard.nextInt();
		
		//creating game object from superclass
		Game game1 = new Game(gameName, numPlayers);
		
		//creating object 2 from subclass using superclass constructor
		GameWithTimeLimit game2 = new GameWithTimeLimit(gameName, numPlayers, timeLimit);
		
		//printing game objects toStrings to the screen
		System.out.println("\n" + game1);
		System.out.println("");
		System.out.println(game2);
		
		//closing keyboard
		keyboard.close();
	}

}
