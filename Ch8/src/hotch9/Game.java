package hotch9;

public class Game {
	private String gameName;
	private int maxPlayers;
	
	
	public Game(String gameName, int maxPlayers) 
	{
		this.gameName = gameName;
		this.maxPlayers = maxPlayers;
	}
	
	public Game()
	{
		
	}

	public String getGameName() {
		return gameName;
	}

	public void setGameName(String gameName) {
		this.gameName = gameName;
	}

	public int getMaxPlayers() {
		return maxPlayers;
	}

	public void setMaxPlayers(int maxPlayers) {
		this.maxPlayers = maxPlayers;
	}
	
	public String toString()
	{
		return "Game Name: " + gameName + "\nNumber of Players: " + maxPlayers;
	}
}
