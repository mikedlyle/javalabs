package hotch9;

public class GameWithTimeLimit extends Game {
	private int timeLimit;

	public GameWithTimeLimit(String gameName, int maxPlayers, int timeLimit) 
	{
		super(gameName, maxPlayers);
		this.timeLimit = timeLimit;
	}
	
	public GameWithTimeLimit()
	{
		
	}

	public int getTimeLimit() {
		return timeLimit;
	}

	public void setTimeLimit(int timeLimit) {
		this.timeLimit = timeLimit;
	}
	
	public String toString()
	{
		return super.toString() + "\nTime Limit: " + timeLimit;
	}

}
