package ex1;

public class ProductionWorker extends Employee {
	private int shift;
	private double payRate;
	
	public int DAY_SHIFT = 1;
	public int NIGHT_SHIFT = 2;
	
	public ProductionWorker(String name, String num, String date, int shift, double rate)
	{
		super(name, num, date);
		this.payRate = rate;
		this.shift = shift;
	}
	
	public ProductionWorker()
	{
		
	}

	public int getShift() {
		return shift;
	}

	public void setShift(int shift) {
		this.shift = shift;
	}

	public double getPayRate() {
		return payRate;
	}

	public void setPayRate(double payRate) {
		this.payRate = payRate;
	}
	
	public String toString()
	{
		String str = "";
		
		if(shift == 1)
		{
			str = "Day Shift";
		}
		else if(shift == 2)
		{
			str = "Night Shift";
		}
		
		return super.toString() + "Shift: " + str + "\nPay Rate: " + payRate;
	}
}
