package ex1;
import java.util.Scanner;

public class EmployeeDriver {

	public static void main(String[] args) {
		Scanner keyboard = new Scanner(System.in);
		String input = "123-1";
		
		ProductionWorker worker1 = new ProductionWorker();
		
		while(worker1.isValidEmpNum(input) == false);
		{
			System.out.println("What is your employee number?");
			input = keyboard.nextLine();
			worker1.setEmployeeNumber(input);
		}
		
		System.out.println(worker1.getEmployeeNumber());
		System.out.println(worker1);
		keyboard.close();
	}
}
