package ex1;

public class Employee {
	private String name;
	private String employeeNumber;
	private String hireDate;
	
	public Employee(String name, String num, String date)
	{
		this.name = name;
		this.employeeNumber = num;
		this.hireDate = date;
	}
	
	public Employee()
	{
		
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getEmployeeNumber() {
		return employeeNumber;
	}

	public void setEmployeeNumber(String employeeNumber) {
		this.employeeNumber = employeeNumber;
	}

	public String getHireDate() {
		return hireDate;
	}

	public void setHireDate(String hireDate) {
		this.hireDate = hireDate;
	}
	
	public boolean isValidEmpNum(String e)

	{
		boolean validation = true;
			if ((!Character.isDigit(e.charAt(0))) || (!Character.isDigit(e.charAt(1))) || (!Character.isDigit(e.charAt(2))) || (e.charAt(3) != '-') || (!Character.isLetter(e.charAt(4))) || (!(e.charAt(4)>= 'A' && e.charAt(4)<= 'M')))
	            { 
	               validation = false;
	            }
		return validation;
	}
	
	public String toString()
	{
		return "Name: " + name + "\nEmployee Number: " + employeeNumber + "\nHire Date: " + hireDate + "\n";
	}
}
