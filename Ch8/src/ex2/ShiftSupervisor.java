package ex2;

public class ShiftSupervisor extends Employee{
	private double salary;
	private double bonus;
	
	public ShiftSupervisor(String name, String num, String date, double sal, double b)
	{
		super(name, num, date);
		this.salary = sal;
		this.bonus = b;
	}
	
	public ShiftSupervisor() 
	{
		
	}

	public double getSalary() {
		return salary;
	}

	public void setSalary(double salary) {
		this.salary = salary;
	}

	public double getBonus() {
		return bonus;
	}

	public void setBonus(double bonus) {
		this.bonus = bonus;
	}
	
	public String toString()
	{
		return super.toString() + (String.format("\nSalary: $%,.2f \nBonus: $%,.2f", salary,bonus));
	}
}