package ex2;
import java.util.Scanner;
public class ShiftDriver {

	public static void main(String[] args) {
		Scanner keyboard = new Scanner(System.in);
		
		String name;
		double sal;
		double bon;
		
		System.out.println("Enter the name for the supervisor: ");
		name = keyboard.nextLine();
		
		System.out.println("Enter the salary for the supervisor: ");
		sal = keyboard.nextDouble();
		
		System.out.println("Enter the bonus for the supervisor: ");
		bon = keyboard.nextDouble();

		ShiftSupervisor sup1 = new ShiftSupervisor(name, "123-A", "1/11/11", sal, bon);
		
		System.out.println(sup1);
		
		keyboard.close();
	}

}
